#!/bin/bash
zenity --question \
	   --title=Afanasy \
	   --text=Você\ deseja\ ativar\ ou\ desativar\ o\ servidor\ do\ Afanasy\? \
	   --ok-label=Ativar\ o\ servidor \
	   --cancel-label=Desativar\ o\ servidor

case $? in
0)
echo "Iniciando o servidor..."
gksu -- bash -c ' \
/etc/init.d/avahi-daemon restart; \
/etc/init.d/afserver stop; \
cp /opt/afanasy-services/afserver.service /etc/avahi/services/; \
/etc/init.d/afserver start; \
/etc/init.d/avahi-daemon restart; \
mount -t cifs -o username=nobody,password=123456 //127.0.0.1/Videos /srv/Vídeos/; \
sh /opt/afanasy-services/permissao.sh'
zenity --info --text=Servidor\ ativado\ com\ sucesso\!
;;
1)
echo "Encerrando o servidor..."
gksu -- bash -c ' \
/etc/init.d/afserver stop; \
rm /etc/avahi/services/afserver.service; \
/etc/init.d/avahi-daemon restart; \
rm /opt/afanasy-services/node-scripts/who; \
umount -f -R /srv/Vídeos/'
zenity --info --text=Servidor\ desativado\ com\ sucesso\!
;;
-1)
echo "Aconteceu um erro inesperado."
zenity --info --text=Aconteceu\ um\ erro\ inesperado\.
;;
esac
zenity --question \
	   --title=Afanasy \
	   --text=Você\ deseja\ ativar\ ou\ desativar\ o\ renderizador\ do\ Afanasy\? \
	   --ok-label=Ativar\ o\ renderizador \
	   --cancel-label=Desativar\ o\ renderizador

case $? in
0)
echo "Iniciando o renderizador..."
gksu -- bash -c ' \
/etc/init.d/afrender stop; \
/etc/init.d/afrender start'
zenity --info --text=Renderizador\ ativado\ com\ sucesso\!
;;
1)
echo "Encerrando o renderizador..."
gksu -- bash -c ' \
/etc/init.d/afrender stop'
zenity --info --text=Renderizador\ desativado\ com\ sucesso\!
;;
-1)
echo "Aconteceu um erro inesperado."
zenity --info --text=Aconteceu\ um\ erro\ inesperado\.
;;
esac
