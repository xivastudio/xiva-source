#!/bin/bash

#criar e montar pasta Home do servidor nos nodes
who=$(cat /opt/afanasy-services/node-scripts/who)
mkdir /home/$who
chmod 777 /home/$who
sudo mount -t cifs -o username=nobody,password=123456,uid=1000 //`cat /tmp/afserver-ip`/home /home/$who

#Montar pastas de Hds Externos + fonts do servidor nos nodes
sudo mount -t cifs -o username=nobody,password=123456,uid=1000 //`cat /tmp/afserver-ip`/Armazenamento\ externo /media
sudo mount -t cifs -o username=nobody,password=123456,uid=1000 //`cat /tmp/afserver-ip`/Fontes /usr/local/share/fonts