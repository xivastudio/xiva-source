#!/bin/bash

exec 2> >(tee -a /tmp/postinst-user-pos.log)
exec 1>&2

mkdir -p ~/.config/gtk-3.0
cat ~/.config/user-dirs.dirs | tail -n 7 | grep -vi desktop | cut -d "/" -f 2 | cut -d '"' -f 1 | sed "s/^/file:\/\/\/home\/$USER\//" > ~/.config/gtk-3.0/bookmarks

#detectar se é maquina virtual e dar alerta
vm=$(lspci | grep -i virtualbox)
if [ -n "$vm" ]; then
	sleep 10
	zenity --info --title="Maquina Virtual" --text="Maquina Virtual detectada.\nAlguns recursos podem não funcionar de forma correta.\n\nRecomendamos testar o XiVa Studio em modo Live." --width=400 2> /dev/null
fi

#transparency gnome-terminal
dconf write /org/gnome/terminal/legacy/profiles:/:b1dcc9dd-5262-4d8d-a863-c897e6d979b9/use-theme-transparency false
dconf write /org/gnome/terminal/legacy/profiles:/:b1dcc9dd-5262-4d8d-a863-c897e6d979b9/use-transparent-background true
dconf write /org/gnome/terminal/legacy/profiles:/:b1dcc9dd-5262-4d8d-a863-c897e6d979b9/background-transparency-percent 10

#pastas do dolphin
dirs=$(cat ~/.config/user-dirs.dirs | tail -n 8 | cut -d "/" -f 2 | cut -d '"' -f 1 | sed "s/^/file:\/\/\/home\/$USER\//")
nf=$(cat ~/.config/user-dirs.dirs | tail -n 8 | cut -d "/" -f 2 | cut -d '"' -f 1)

echo '<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xbel>
<xbel xmlns:mime="http://www.freedesktop.org/standards/shared-mime-info" xmlns:bookmark="http://www.freedesktop.org/standards/desktop-bookmarks" xmlns:kdepriv="http://www.kde.org/kdepriv">
 <bookmark href=''"'"$(echo file:\/\/\/home\/$USER)"'"''>
  <title>Home</title>
  <info>
   <metadata owner="http://freedesktop.org">
    <bookmark:icon name="user-home"/>
   </metadata>
   <metadata owner="http://www.kde.org">
    <ID>1550679929/0</ID>
    <isSystemItem>true</isSystemItem>
    <IsHidden>false</IsHidden>
   </metadata>
  </info>
 </bookmark>
 <bookmark href=''"'"$(echo $dirs | awk '{print $1, $2, $3}')"'"''>
  <title>'"$(echo $nf | awk '{print $1, $2, $3}')"'</title>
  <info>
   <metadata owner="http://freedesktop.org">
    <bookmark:icon name="user-desktop"/>
   </metadata>
   <metadata owner="http://www.kde.org">
    <ID>1550679929/1</ID>
    <isSystemItem>true</isSystemItem>
    <IsHidden>false</IsHidden>
   </metadata>
  </info>
 </bookmark>
 <bookmark href=''"'"$(echo $dirs | awk '{print $7}')"'"''>
  <title>'"$(echo $nf | awk '{print $7}')"'</title>
  <info>
   <metadata owner="http://freedesktop.org">
    <bookmark:icon name="folder-documents"/>
   </metadata>
   <metadata owner="http://www.kde.org">
    <ID>1550680417/0</ID>
    <isSystemItem>false</isSystemItem>
    <IsHidden>false</IsHidden>
   </metadata>
  </info>
 </bookmark>
 <bookmark href=''"'"$(echo $dirs | awk '{print $4}')"'"''>
  <title>'"$(echo $nf | awk '{print $4}')"'</title>
  <info>
   <metadata owner="http://freedesktop.org">
    <bookmark:icon name="folder-downloads"/>
   </metadata>
   <metadata owner="http://www.kde.org">
    <ID>1550679929/2</ID>
    <isSystemItem>true</isSystemItem>
    <IsHidden>false</IsHidden>
   </metadata>
  </info>
 </bookmark>
 <bookmark href=''"'"$(echo $dirs | awk '{print $8}')"'"''>
  <title>'"$(echo $nf | awk '{print $8}')"'</title>
  <info>
   <metadata owner="http://freedesktop.org">
    <bookmark:icon name="folder-music"/>
   </metadata>
   <metadata owner="http://www.kde.org">
    <ID>1550680540/1</ID>
    <isSystemItem>false</isSystemItem>
    <IsHidden>false</IsHidden>
   </metadata>
  </info>
 </bookmark>
 <bookmark href=''"'"$(echo $dirs | awk '{print $9}')"'"''>
  <title>'"$(echo $nf | awk '{print $9}')"'</title>
  <info>
   <metadata owner="http://freedesktop.org">
    <bookmark:icon name="folder-pictures"/>
   </metadata>
   <metadata owner="http://www.kde.org">
    <ID>1550680548/2</ID>
    <isSystemItem>false</isSystemItem>
    <IsHidden>false</IsHidden>
   </metadata>
  </info>
 </bookmark>
 <bookmark href=''"'"$(echo $dirs | awk '{print $6}')"'"''>
  <title>'"$(echo $nf | awk '{print $6}')"'</title>
  <info>
   <metadata owner="http://freedesktop.org">
    <bookmark:icon name="folder"/>
   </metadata>
   <metadata owner="http://www.kde.org">
    <ID>1550680551/3</ID>
    <isSystemItem>false</isSystemItem>
    <IsHidden>false</IsHidden>
   </metadata>
  </info>
 </bookmark>
 <bookmark href=''"'"$(echo $dirs | awk '{print $5}')"'"''>
  <title>'"$(echo $nf | awk '{print $5}')"'</title>
  <info>
   <metadata owner="http://freedesktop.org">
    <bookmark:icon name="folder"/>
   </metadata>
   <metadata owner="http://www.kde.org">
    <ID>1550680553/4</ID>
    <isSystemItem>false</isSystemItem>
    <IsHidden>false</IsHidden>
   </metadata>
  </info>
 </bookmark>
 <bookmark href=''"'"$(echo $dirs | awk '{print $10}')"'"''>
  <title>'"$(echo $nf | awk '{print $10}')"'</title>
  <info>
   <metadata owner="http://freedesktop.org">
    <bookmark:icon name="folder-videos"/>
   </metadata>
   <metadata owner="http://www.kde.org">
    <ID>1550680556/5</ID>
    <isSystemItem>false</isSystemItem>
    <IsHidden>false</IsHidden>
   </metadata>
  </info>
 </bookmark>
 <bookmark href="remote:/">
  <title>Network</title>
  <info>
   <metadata owner="http://freedesktop.org">
    <bookmark:icon name="folder-network"/>
   </metadata>
   <metadata owner="http://www.kde.org">
    <ID>1550679929/3</ID>
    <isSystemItem>true</isSystemItem>
    <IsHidden>false</IsHidden>
   </metadata>
  </info>
 </bookmark>
 <bookmark href="file:///">
  <title>Root</title>
  <info>
   <metadata owner="http://freedesktop.org">
    <bookmark:icon name="folder-root"/>
   </metadata>
   <metadata owner="http://www.kde.org">
    <ID>1550679929/4</ID>
    <isSystemItem>true</isSystemItem>
    <IsHidden>false</IsHidden>
   </metadata>
  </info>
 </bookmark>
 <bookmark href="trash:/">
  <title>Trash</title>
  <info>
   <metadata owner="http://freedesktop.org">
    <bookmark:icon name="user-trash"/>
   </metadata>
   <metadata owner="http://www.kde.org">
    <ID>1550679929/5</ID>
    <isSystemItem>true</isSystemItem>
    <IsHidden>false</IsHidden>
   </metadata>
  </info>
 </bookmark>
 <info>
  <metadata owner="http://www.kde.org">
   <GroupState-Places-IsHidden>false</GroupState-Places-IsHidden>
   <GroupState-Remote-IsHidden>false</GroupState-Remote-IsHidden>
   <GroupState-Devices-IsHidden>false</GroupState-Devices-IsHidden>
   <GroupState-RemovableDevices-IsHidden>false</GroupState-RemovableDevices-IsHidden>
  </metadata>
 </info>' > /home/$USER/.local/share/user-places.xbel

#apagar e remover esse script do boot de user
rm ~/.config/autostart/postinst-user-pos.desktop
rm ~/postinst-user-pos.sh
