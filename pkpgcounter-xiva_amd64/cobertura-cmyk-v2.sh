#!/bin/bash
#foi usado como base de conhecimento o script feito por Adriano H. Hedler (https://www.vivaolinux.com.br/script/Calculo-de-area-de-cobertura-de-tonertinta-em-uma-impressao)

#depende do pacote pkpgcounter instalado
#para instalar em debian e ubuntu derivados:
#sudo apt update && sudo apt install pkpgcounter

# Forma de funcionamento:
# ./cobertura-cmyk.sh arquivo.pdf

#calculo de custo de impressão, basedo nos valores fornecidos pelo fabricante.
#ex: a impressora faz 6.000 copias com 5% de cobertura, se for 10% faz 10.000 copias e assim por diante.
#o cartucho dela custa R$150,00 ou seja 150/6000=0,025 por copia com 5% de cobertura.
#o calculo se dá: porcentagem / 5 * valor por pagina.
#ex: uma impressão com 32% de cobertura fica: 32/5*0,025=R$0,16 por pagina


pkpgcounter -c cmyk "$1" > /tmp/countercmyk

paginas=$(pkpgcounter "$1")

ciano=$(cat /tmp/countercmyk | awk '{print $3}' | cut -d "%" -f 1 | awk '{sum=sum+$0} END {print sum}')
magenta=$(cat /tmp/countercmyk | awk '{print $6}' | cut -d "%" -f 1 | awk '{sum=sum+$0} END {print sum}')
amarelo=$(cat /tmp/countercmyk | awk '{print $9}' | cut -d "%" -f 1 | awk '{sum=sum+$0} END {print sum}')
preto=$(cat /tmp/countercmyk | awk '{print $12}' | cut -d "%" -f 1 | awk '{sum=sum+$1} END {print sum}')
#valor do tonner por pagina em 5%
vtonnerhp=0.04
#valor total cor
vtciano=$(echo $ciano/5*$vtonnerhp | bc)
vtmagenta=$(echo $magenta/5*$vtonnerhp | bc)
vtamarelo=$(echo $amarelo/5*$vtonnerhp | bc)
vtpreto=$(echo $preto/5*$vtonnerhp | bc)

echo -n "Esse arquivo contem" $paginas; echo " paginas"
echo -n "O Percentual de cobertura de Ciano é: "$(echo "scale=2;"$ciano/$paginas"" | bc); echo "%" 
echo -n "O Percentual de cobertura de Magenta é: "$(echo "scale=2;"$magenta/$paginas"" | bc); echo "%" 
echo -n "O Percentual de cobertura de Amarelo é: "$(echo "scale=2;"$amarelo/$paginas"" | bc); echo "%" 
echo -n "O Percentual de cobertura de Preto é: "$(echo "scale=2;"$preto/$paginas"" | bc); echo "%" 
echo
echo -n "O valor de impressão Cyano na Impressora HP é de: "$(echo "$ciano/$paginas/5*$vtonnerhp" | bc); echo " centavos por pagina"; echo " dando um total de R$"$vtciano""
echo -n "O valor de impressão Magenta na Impressora HP é de: "$(echo "$magenta/$paginas/5*$vtonnerhp" | bc); echo " centavos por pagina"; echo " dando um total de R$"$vtmagenta""
echo -n "O valor de impressão Amarelo na Impressora HP é de: "$(echo "$amarelo/$paginas/5*$vtonnerhp" | bc); echo " centavos por pagina"; echo " dando um total de R$"$vtamarelo""
echo -n "O valor de impressão Preto na Impressora HP é de: "$(echo "$preto/$paginas/5*$vtonnerhp" | bc); echo " centavos por pagina"; echo " dando um total de R$"$vtpreto""

echo -n "Dando um Valor total da impressão de R$"$(echo "$vtciano+$vtmagenta+$vtamarelo+$vtpreto" | bc)""
echo