#!/bin/bash

#deixar as respostas bash em ingles
#export LC_ALL=C

#abrir caixa de dialogo perguntando se:
#quer add um modulo
#quer reiniar o pulse
#quer retirar um modulo - esse fica pra depois

zenity --width=300 --question \
--title="Mixagem Estereo" \
--text "Deseja Ligar ou Desligar Mixagem Estéreo?" \
--ok-label="Desligar" \
--cancel-label="Ligar"

foo=$?

if [[ $foo -eq 1 ]]; then
	pactl load-module module-loopback
elif [[ $foo -eq 0 ]]; then
	pactl unload-module module-loopback
fi
