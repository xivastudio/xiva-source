#!/bin/bash

#criar log em /var/log/xiva-install-low-latency.log
exec 2> >(tee -a /var/log/xiva-install-low-latency.log)
exec 1>&2

#verificar conexão com a internet
net=$(ping -c 1 8.8.8.8 | cut -d " " -f 1 | grep 64)
#verificar kernel generic instalado
generic=$(ls /boot | grep generic)

#caso não esteja conectado
if [ "$net" != "64" ]; then
	zenity --width=400 --info --title="Conexão com Internet" --text="Não conectado a Internet\nPor favor verifique a sua conexão e tente novamente." 2> /dev/null
	exit 0
fi

#comandos a serem executados
(
# =================================================================
echo "# Atualizando." ; sleep 2
#update de repositorio
apt update
# Command for first task goes on this line.

# =================================================================
echo "25"
echo "# Instalando Modo de recuperação.\nEsse operação pode demorar muitos minutos." ; sleep 2
apt install linux-generic -y
#verificar se generic instalado
vergen=$(dpkg --list | grep linux-image-generic)
if [ -z "$vergen" ]; then
  zenity --width=400 --info --title="ERROR" --text="Algo de errado não deu certo.\nVeja o log em /var/log/xiva-install-low-latency.log\nQualquer duvida entre em contato com o Suporte." 2> /dev/null
  exit 0
fi
# Command for second task goes on this line.

# =================================================================
echo "50"
echo "# Instalando Modo multi-tarefa de Baixa lantencia.\nEsse operação pode demorar muitos minutos." ; sleep 2
apt install linux-lowlatency -y
#verificar se liquorix foi instalado
verlow=$(dpkg --list | grep linux-image-low-latency)
if [ -z "$verlow" ]; then
  zenity --width=400 --info --title="ERROR" --text="Algo de errado não deu certo.\nVeja o log em /var/log/xiva-install-low-latency.log\nQualquer duvida entre em contato com o Suporte." 2> /dev/null
  exit 0
fi
# Command for third task goes on this line.

# =================================================================
echo "75"
echo "# Removendo outros Modos." ; sleep 2
#zenity --width=400 --info --title="Instalação Modo Multi-tarefa" --text="Por favor <b>não feche</b> a Janela de texto.\n\n<b>Se aparecer uma tela de pergunta, responda NÃO</b>.\n\nPressione OK para iniciar a remoção de outros Modos." 2> /dev/null
#if [ -n "$mt" ]; then
# gnome-terminal -- apt remove --purge *lowlatency* -y
DEBIAN_FRONTEND=noninteractive apt remove --purge *liquorix* -y
#else
#	zenity --width=400 --info --title="ERROR" --text="Algo deu errado.\nPor Favor entre em contato com o Suporte." 2> /dev/null
#fi
# Command for fourth task goes on this line.

# =================================================================
echo "99"
echo "# Terminado." ; sleep 2
# Command for fifth task goes on this line.

#executar comandos e abrir caixa de progresso
) |
zenity --width=400 --progress \
  --title="Instalando Modo multi-tarefa" \
  --text="First Task." \
  --width="300" \
  --percentage=0 \
  --auto-close \
  --auto-kill 

zenity --width=400 --info --title="Instalação Modo Multi-tarefa Low-Latency" --text="Modo Multi-tarefa Low-Latency instalado com sucesso.\nPor favor reinicie o computador para ter efeito." --width=350 2> /dev/null

exit 0
