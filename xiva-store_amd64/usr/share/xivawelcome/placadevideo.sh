#!/bin/bash

placa=$(lspci | grep VGA | cut -d ":" -f 3 | cut -d "(" -f 1)

zenity --width=600 --info --title="Qual a minha placa de video?" --text="$placa"