#!/bin/bash

#criar log em /var/log/xiva-install-nvidia.log
exec 2> >(tee -a /var/log/xiva-install-nvidia.log)
exec 1>&2

lock=$(ps -A | grep apt)
if [ -n "$lock" ]; then
	zenity --width=300 --info --title="Apt Aberto" --text="Algum programa de instalação está aberto no momento, feche-o e tente novamente.\n \n Obrigad@"
	exit 0
fi

net=$(ping -c 1 8.8.8.8 | cut -d " " -f 1 | grep 64)

if [ "$net" != "64" ]; then
	zenity --width=300 --info --title="Conexão com Internet" --text="Não conectado a Internet\nPor favor verifique a sua conexão e tente novamente." 2> /dev/null
	exit 0
fi

zenity --width=300 --question \
--title="Adicinar / Remover Fontes Alternativas" \
--text "Deseja adicionar ou remover fontes alternativas de drivers Nvidia?" \
--ok-label="Remover" \
--cancel-label="Adicionar"

foo=$?

if [[ $foo -eq 1 ]]; then
  (
  add-apt-repository -y ppa:graphics-drivers/ppa
  #apt update
  ) |
  #echo "adicionada" |
  zenity --width=300 --progress --pulsate \
  --title="Adicinar Fontes Alternativas" \
  --text "Adicionando Fontes Alternativas Nvidia" \
  --percentage=0 \
  --auto-close \
  --auto-kill

  zenity --width=300 --info --title="Gerenciador de Drivers" --text="Abra o gerenciador de drivers da sua distribuição e instale os drivers da nvidia disponiveis da nova fonte." 2> /dev/null


elif [[ $foo -eq 0 ]]; then
  (
  add-apt-repository -y -r ppa:graphics-drivers/ppa
  #apt update
  ) |
  zenity --width=300 --progress --pulsate \
  --title="Removendo Fontes Alternativas" \
  --text "Removendo Fontes Alternativas Nvidia" \
  --percentage=0 \
  --auto-close \
  --auto-kill

  zenity --width=300 --info --title="Gerenciador de Drivers" --text="Abra o gerenciador de drivers da sua distribuição e instale os drivers da nvidia disponiveis da fonte antiga." 2> /dev/null

fi
