	#!/bin/bash

#criar log em /var/log/xiva-install-low-latency-liquorix.log
exec 2> >(tee -a /var/log/xiva-install-low-latency-liquorix.log)
exec 1>&2

#verificar conexão com a internet
net=$(ping -c 1 8.8.8.8 | cut -d " " -f 1 | grep 64)
#verificar kernel generic instalado
generic=$(ls /boot | grep generic)

#caso não esteja conectado
if [ "$net" != "64" ]; then
	zenity --width=400 --info --title="Conexão com Internet" --text="Não conectado a Internet\nPor favor verifique a sua conexão e tente novamente." 2> /dev/null
	exit 0
fi

#mostra grub no boot
sed -i '/GRUB_HIDDEN_TIMEOUT=/{s/^/#/}' /etc/default/grub

#comandos a serem executados
(
# =================================================================
echo "# Atualizando." ; sleep 2
#add ppa do liquorix
add-apt-repository -y ppa:damentz/liquorix
# Command for first task goes on this line.

# =================================================================
echo "25"
echo "# Instalando Modo de recuperação.\nEsse operação pode demorar muitos minutos." ; sleep 2
apt install linux-generic -y
#verificar se generic instalado
vergen=$(dpkg --list | grep linux-image-generic)
if [ -z "$vergen" ]; then
  zenity --width=400 --info --title="ERROR" --text="Algo de errado não deu certo.\nVeja o log em /var/log/xiva-install-low-latency-liquorix.log\nQualquer duvida entre em contato com o Suporte." 2> /dev/null
  exit 0
fi
# Command for second task goes on this line.

# =================================================================
echo "50"
echo "# Instalando Modo Misto.\nEsse operação pode demorar muuuuuitos minutos." ; sleep 2
apt install --reinstall -y linux-image-liquorix-amd64 linux-headers-liquorix-amd64 linux-lowlatency
#verificar se liquorix foi instalado
verliqlow=$(dpkg --list | grep -E "linux-image-liquorix|linux-image-lowlatency")
if [ -z "$verliqlow" ]; then
  zenity --width=400 --info --title="ERROR" --text="Algo de errado não deu certo.\nVeja o log em /var/log/xiva-install-low-latency-liquorix.log\nQualquer duvida entre em contato com o Suporte." 2> /dev/null
  exit 0
fi
# Command for third task goes on this line.

# =================================================================
echo "75"
echo "# Removendo outros Modos." ; sleep 2
# Command for fourth task goes on this line.

# =================================================================
echo "99"
echo "# Terminado." ; sleep 2
# Command for fifth task goes on this line.

#executar comandos e abrir caixa de progresso
) |
zenity --width=400 --progress \
  --title="Instalando Modo Misto" \
  --text="First Task." \
  --width="400" \
  --percentage=0 \
  --auto-close \
  --auto-kill 

zenity --width=400 --info --title="Instalação Modo Misto" --text="\n\n<b>Por Favor reinicie o seu computador para ter efeito</b>\n\nNa inicialização escolha a opção <b>avançada</b>\nEscolha o <b>Low-latency</b> para modo Multi-Tarefa\nEscolha o <b>Liquorix</b> para modo Mono-Tarefa.\n\nQualquer duvida clique em Saiba Mais a Respeito\nou entre em contato conosco pelo Suporte" --width=450 2> /dev/null