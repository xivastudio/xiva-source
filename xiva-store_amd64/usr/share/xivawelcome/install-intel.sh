#!/bin/bash

lock=$(ps -A | grep apt)

if [ -n "$lock" ]; then
	zenity --width=300 --info --title="Apt Aberto" --text="Algum programa de instalação está aberto no momento, feche-o e tente novamente.\n \n Obrigad@"
	exit 0
fi

net=$(ping -c 1 8.8.8.8 | cut -d " " -f 1 | grep 64)

if [ "$net" != "64" ]; then
	zenity --width=300 --info --title="Conexão com Internet" --text="Não conectado a Internet\nPor favor verifique a sua conexão e tente novamente." 2> /dev/null
	exit 0
fi

zenity --width=300 --question \
--title="Adicinar / Remover Fontes Alternativas" \
--text "Deseja adicionar ou remover fontes alternativas de drivers intel?" \
--ok-label="Remover" \
--cancel-label="Adicionar"

foo=$?

if [[ $foo -eq 1 ]]; then
  (
  add-apt-repository -y ppa:oibaf/graphics-drivers
  #apt update
  ) |
  #echo "adicionada" |
  zenity --width=300 --progress --pulsate \
  --title="Adicinar Fontes Alternativas" \
  --text "Adicionando Fontes Alternativas intel" \
  --percentage=0 \
  --auto-close \
  --auto-kill

  zenity --width=300 --info --title="Gerenciador de Drivers" --text="Abra o gerenciador de upgrade da sua distribuição e atualize o sistema para obter o driver intel disponivel da nova fonte." 2> /dev/null


elif [[ $foo -eq 0 ]]; then
  (
 apt update
 ppa-purge -y ppa:oibaf/graphics-drivers
  #apt update
  ) |
  zenity --width=300 --progress --pulsate \
  --title="Removendo Fontes Alternativas" \
  --text "Removendo Fontes Alternativas intel" \
  --percentage=0 \
  --auto-close \
  --auto-kill

  zenity --width=300 --info --title="Gerenciador de Drivers" --text="Drivers removidos, reinicie o computador." 2> /dev/null

fi











#add-apt-repository -y ppa:oibaf/graphics-drivers | \
#zenity --width=300 --progress \
#        --title "Drives Intel" \
# 		--text="Adicionando Fontes" \
# 		--width="300" \
#        --pulsate \
#        --auto-close \
#        --auto-kill
#
##zenity --width=300 --info --title="Intalação Intel Alternativa" --text="O sistema está atualizando as fontes alternetivas.\nAguarde alguns instantes que o instalado de drivers abrira.\nPode demorar alguns minitos dependendo da velocidade da sua conexão com a internet.\n\n Obrigad@"
#
##apt update
##apt install -y oibaf-graphics-drivers-ppa
##add-apt-repository -y ppa:oibaf/graphics-drivers
##xterm -e "apt update"
##apt upgrade -y
##software-properties-gtk --open-tab=4
#plasma-discover --mode update
#killall aptd
#
#exit 0