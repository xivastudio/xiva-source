#!/bin/bash

#deixar as respostas bash em ingles
#export LC_ALL=C

#abrir caixa de dialogo perguntando se:
#quer add um modulo
#quer reiniar o pulse
#quer retirar um modulo - esse fica pra depois

zenity --width=300 --question \
--title="Pulseaudio Virtual Sink" \
--text "Deseja adicionar ou reiniciar o Pulseaudio Virtual Sink?" \
--ok-label="Reiniciar" \
--cancel-label="Adicionar"

foo=$?

addvs () {
	#perguntar nome do Virutal sink
	vs=$(zenity --title="Virtual Sink" --text "digite o nome do novo virtual sink" --entry)
	#adicionar virtual sink
	pactl load-module module-null-sink sink_name=$vs sink_properties=device.description="$vs"
	#informe
	zenity --width=300 --question \
	--title="Pulseaudio Virtual Sink" \
	--text "Aadicionar outro Virtual Sink?" \
	--ok-label="Continuar" \
	--cancel-label="Adicionar"
	foo=$?
	if [[ $foo -eq 1 ]]; then
		addvs
	elif [[ $foo -eq 0 ]]; then
		zenity --width=300 --info --title="Pulseaudio Virtual Sink" --text="\nInicie o aplicativo emisor de audio para alterar os Virtual Sink." 2> /dev/null
		#abrir pavucontol na aba 1
		pavucontrol -t 1
	fi
}

if [[ $foo -eq 1 ]]; then
	addvs
elif [[ $foo -eq 0 ]]; then
	pulseaudio -k
	zenity --width=200 --info --title="Pulseaudio" --text="\nPulseaudio Reiniciado." 2> /dev/null

fi