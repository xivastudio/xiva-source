/****************************************************************************
** Meta object code from reading C++ file 'dialog.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../../watch/dialog.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'dialog.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_Dialog[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      12,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
       8,    7,    7,    7, 0x05,

 // slots: signature, parameters, type, tag, flags
      19,   15,    7,    7, 0x08,
      40,    7,    7,    7, 0x08,
      57,    7,    7,    7, 0x08,
      72,    7,    7,    7, 0x08,
      84,    7,    7,    7, 0x08,
     103,    7,    7,    7, 0x08,
     130,    7,    7,    7, 0x08,
     149,    7,    7,    7, 0x08,
     173,    7,    7,    7, 0x08,
     194,    7,    7,    7, 0x08,
     222,  216,    7,    7, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_Dialog[] = {
    "Dialog\0\0stop()\0msg\0newMessage(af::Msg*)\0"
    "connectionLost()\0repaintWatch()\0"
    "actColors()\0actNotifications()\0"
    "actSavePreferencesOnExit()\0"
    "actSaveGUIOnExit()\0actSaveWndRectsOnExit()\0"
    "actSavePreferences()\0actShowOfflineNoise()\0"
    "theme\0actGuiTheme(QString)\0"
};

void Dialog::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        Dialog *_t = static_cast<Dialog *>(_o);
        switch (_id) {
        case 0: _t->stop(); break;
        case 1: _t->newMessage((*reinterpret_cast< af::Msg*(*)>(_a[1]))); break;
        case 2: _t->connectionLost(); break;
        case 3: _t->repaintWatch(); break;
        case 4: _t->actColors(); break;
        case 5: _t->actNotifications(); break;
        case 6: _t->actSavePreferencesOnExit(); break;
        case 7: _t->actSaveGUIOnExit(); break;
        case 8: _t->actSaveWndRectsOnExit(); break;
        case 9: _t->actSavePreferences(); break;
        case 10: _t->actShowOfflineNoise(); break;
        case 11: _t->actGuiTheme((*reinterpret_cast< QString(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData Dialog::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject Dialog::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_Dialog,
      qt_meta_data_Dialog, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &Dialog::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *Dialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *Dialog::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_Dialog))
        return static_cast<void*>(const_cast< Dialog*>(this));
    return QWidget::qt_metacast(_clname);
}

int Dialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 12)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 12;
    }
    return _id;
}

// SIGNAL 0
void Dialog::stop()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}
QT_END_MOC_NAMESPACE
