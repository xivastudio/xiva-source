/****************************************************************************
** Meta object code from reading C++ file 'listtasks.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../../watch/listtasks.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'listtasks.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_ListTasks[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      17,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      19,   11,   10,   10, 0x05,

 // slots: signature, parameters, type, tag, flags
      42,   10,   10,   10, 0x08,
      60,   10,   10,   10, 0x08,
      81,   10,   10,   10, 0x08,
      97,   10,   10,   10, 0x08,
     115,   10,   10,   10, 0x08,
     133,   10,   10,   10, 0x08,
     168,  150,   10,   10, 0x08,
     193,   10,   10,   10, 0x08,
     207,   10,   10,   10, 0x08,
     227,  220,   10,   10, 0x08,
     246,   10,   10,   10, 0x08,
     282,  266,   10,   10, 0x08,
     306,   10,   10,   10, 0x08,
     322,   10,   10,   10, 0x08,
     340,   10,   10,   10, 0x08,
     355,   10,   10,   10, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_ListTasks[] = {
    "ListTasks\0\0type,id\0changeMonitor(int,int)\0"
    "actBlockCommand()\0actBlockWorkingDir()\0"
    "actBlockFiles()\0actBlockCmdPost()\0"
    "actBlockService()\0actBlockParser()\0"
    "id_block,i_action\0blockAction(int,QString)\0"
    "actTaskInfo()\0actTaskLog()\0number\0"
    "actTaskStdOut(int)\0actTaskErrorHosts()\0"
    "num_cmd,num_img\0actTaskPreview(int,int)\0"
    "actTaskListen()\0actTasksRestart()\0"
    "actTasksSkip()\0actBrowseFolder()\0"
};

void ListTasks::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        ListTasks *_t = static_cast<ListTasks *>(_o);
        switch (_id) {
        case 0: _t->changeMonitor((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 1: _t->actBlockCommand(); break;
        case 2: _t->actBlockWorkingDir(); break;
        case 3: _t->actBlockFiles(); break;
        case 4: _t->actBlockCmdPost(); break;
        case 5: _t->actBlockService(); break;
        case 6: _t->actBlockParser(); break;
        case 7: _t->blockAction((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 8: _t->actTaskInfo(); break;
        case 9: _t->actTaskLog(); break;
        case 10: _t->actTaskStdOut((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 11: _t->actTaskErrorHosts(); break;
        case 12: _t->actTaskPreview((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 13: _t->actTaskListen(); break;
        case 14: _t->actTasksRestart(); break;
        case 15: _t->actTasksSkip(); break;
        case 16: _t->actBrowseFolder(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData ListTasks::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject ListTasks::staticMetaObject = {
    { &ListItems::staticMetaObject, qt_meta_stringdata_ListTasks,
      qt_meta_data_ListTasks, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ListTasks::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ListTasks::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ListTasks::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ListTasks))
        return static_cast<void*>(const_cast< ListTasks*>(this));
    return ListItems::qt_metacast(_clname);
}

int ListTasks::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = ListItems::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 17)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 17;
    }
    return _id;
}

// SIGNAL 0
void ListTasks::changeMonitor(int _t1, int _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE
