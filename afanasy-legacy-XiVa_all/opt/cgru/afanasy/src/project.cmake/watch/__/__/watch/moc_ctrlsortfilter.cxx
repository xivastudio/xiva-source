/****************************************************************************
** Meta object code from reading C++ file 'ctrlsortfilter.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../../watch/ctrlsortfilter.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'ctrlsortfilter.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_CtrlSortFilter[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      11,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       5,       // signalCount

 // signals: signature, parameters, type, tag, flags
      16,   15,   15,   15, 0x05,
      34,   15,   15,   15, 0x05,
      57,   15,   15,   15, 0x05,
      73,   15,   15,   15, 0x05,
      93,   15,   15,   15, 0x05,

 // slots: signature, parameters, type, tag, flags
     122,  117,   15,   15, 0x08,
     139,   15,   15,   15, 0x08,
     162,  158,   15,   15, 0x08,
     181,   15,   15,   15, 0x08,
     200,   15,   15,   15, 0x08,
     217,  117,   15,   15, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_CtrlSortFilter[] = {
    "CtrlSortFilter\0\0sortTypeChanged()\0"
    "sortDirectionChanged()\0filterChanged()\0"
    "filterTypeChanged()\0filterSettingsChanged()\0"
    "type\0actSortType(int)\0actSortAscending()\0"
    "str\0actFilter(QString)\0actFilterInclude()\0"
    "actFilterMacth()\0actFilterType(int)\0"
};

void CtrlSortFilter::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        CtrlSortFilter *_t = static_cast<CtrlSortFilter *>(_o);
        switch (_id) {
        case 0: _t->sortTypeChanged(); break;
        case 1: _t->sortDirectionChanged(); break;
        case 2: _t->filterChanged(); break;
        case 3: _t->filterTypeChanged(); break;
        case 4: _t->filterSettingsChanged(); break;
        case 5: _t->actSortType((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 6: _t->actSortAscending(); break;
        case 7: _t->actFilter((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 8: _t->actFilterInclude(); break;
        case 9: _t->actFilterMacth(); break;
        case 10: _t->actFilterType((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData CtrlSortFilter::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject CtrlSortFilter::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_CtrlSortFilter,
      qt_meta_data_CtrlSortFilter, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &CtrlSortFilter::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *CtrlSortFilter::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *CtrlSortFilter::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_CtrlSortFilter))
        return static_cast<void*>(const_cast< CtrlSortFilter*>(this));
    return QWidget::qt_metacast(_clname);
}

int CtrlSortFilter::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 11)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 11;
    }
    return _id;
}

// SIGNAL 0
void CtrlSortFilter::sortTypeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}

// SIGNAL 1
void CtrlSortFilter::sortDirectionChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}

// SIGNAL 2
void CtrlSortFilter::filterChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, 0);
}

// SIGNAL 3
void CtrlSortFilter::filterTypeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, 0);
}

// SIGNAL 4
void CtrlSortFilter::filterSettingsChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 4, 0);
}
QT_END_MOC_NAMESPACE
