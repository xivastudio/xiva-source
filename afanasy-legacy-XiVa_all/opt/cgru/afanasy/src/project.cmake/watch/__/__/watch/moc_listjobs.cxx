/****************************************************************************
** Meta object code from reading C++ file 'listjobs.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../../watch/listjobs.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'listjobs.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_ListJobs[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      35,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      10,    9,    9,    9, 0x08,
      22,    9,    9,    9, 0x08,
      36,    9,    9,    9, 0x08,
      49,    9,    9,    9, 0x08,
      65,    9,    9,    9, 0x08,
      78,    9,    9,    9, 0x08,
      93,    9,    9,    9, 0x08,
     115,    9,    9,    9, 0x08,
     136,    9,    9,    9, 0x08,
     160,    9,    9,    9, 0x08,
     176,    9,    9,    9, 0x08,
     198,    9,    9,    9, 0x08,
     212,    9,    9,    9, 0x08,
     224,    9,    9,    9, 0x08,
     244,    9,    9,    9, 0x08,
     261,    9,    9,    9, 0x08,
     275,    9,    9,    9, 0x08,
     290,    9,    9,    9, 0x08,
     307,    9,    9,    9, 0x08,
     328,    9,    9,    9, 0x08,
     369,  351,    9,    9, 0x08,
     394,    9,    9,    9, 0x08,
     405,    9,    9,    9, 0x08,
     415,    9,    9,    9, 0x08,
     428,    9,    9,    9, 0x08,
     447,    9,    9,    9, 0x08,
     467,    9,    9,    9, 0x08,
     487,    9,    9,    9, 0x08,
     504,    9,    9,    9, 0x08,
     525,    9,    9,    9, 0x08,
     536,    9,    9,    9, 0x08,
     554,    9,    9,    9, 0x08,
     566,    9,    9,    9, 0x08,
     582,    9,    9,    9, 0x08,
     609,    9,    9,    9, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_ListJobs[] = {
    "ListJobs\0\0actMoveUp()\0actMoveDown()\0"
    "actMoveTop()\0actMoveBottom()\0actSetUser()\0"
    "actHostsMask()\0actHostsMaskExclude()\0"
    "actMaxRunningTasks()\0actMaxRunTasksPerHost()\0"
    "actDependMask()\0actDependMaskGlobal()\0"
    "actWaitTime()\0actNeedOS()\0actNeedProperties()\0"
    "actPostCommand()\0actLifeTime()\0"
    "actSetHidden()\0actUnsetHidden()\0"
    "actPreviewApproval()\0actNoPreviewApproval()\0"
    "id_block,i_action\0blockAction(int,QString)\0"
    "actStart()\0actStop()\0actRestart()\0"
    "actRestartErrors()\0actRestartRunning()\0"
    "actRestartSkipped()\0actRestartDone()\0"
    "actResetErrorHosts()\0actPause()\0"
    "actRestartPause()\0actDelete()\0"
    "actRequestLog()\0actRequestErrorHostsList()\0"
    "actListenJob()\0"
};

void ListJobs::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        ListJobs *_t = static_cast<ListJobs *>(_o);
        switch (_id) {
        case 0: _t->actMoveUp(); break;
        case 1: _t->actMoveDown(); break;
        case 2: _t->actMoveTop(); break;
        case 3: _t->actMoveBottom(); break;
        case 4: _t->actSetUser(); break;
        case 5: _t->actHostsMask(); break;
        case 6: _t->actHostsMaskExclude(); break;
        case 7: _t->actMaxRunningTasks(); break;
        case 8: _t->actMaxRunTasksPerHost(); break;
        case 9: _t->actDependMask(); break;
        case 10: _t->actDependMaskGlobal(); break;
        case 11: _t->actWaitTime(); break;
        case 12: _t->actNeedOS(); break;
        case 13: _t->actNeedProperties(); break;
        case 14: _t->actPostCommand(); break;
        case 15: _t->actLifeTime(); break;
        case 16: _t->actSetHidden(); break;
        case 17: _t->actUnsetHidden(); break;
        case 18: _t->actPreviewApproval(); break;
        case 19: _t->actNoPreviewApproval(); break;
        case 20: _t->blockAction((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 21: _t->actStart(); break;
        case 22: _t->actStop(); break;
        case 23: _t->actRestart(); break;
        case 24: _t->actRestartErrors(); break;
        case 25: _t->actRestartRunning(); break;
        case 26: _t->actRestartSkipped(); break;
        case 27: _t->actRestartDone(); break;
        case 28: _t->actResetErrorHosts(); break;
        case 29: _t->actPause(); break;
        case 30: _t->actRestartPause(); break;
        case 31: _t->actDelete(); break;
        case 32: _t->actRequestLog(); break;
        case 33: _t->actRequestErrorHostsList(); break;
        case 34: _t->actListenJob(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData ListJobs::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject ListJobs::staticMetaObject = {
    { &ListNodes::staticMetaObject, qt_meta_stringdata_ListJobs,
      qt_meta_data_ListJobs, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ListJobs::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ListJobs::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ListJobs::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ListJobs))
        return static_cast<void*>(const_cast< ListJobs*>(this));
    return ListNodes::qt_metacast(_clname);
}

int ListJobs::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = ListNodes::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 35)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 35;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
