/****************************************************************************
** Meta object code from reading C++ file 'modelnodes.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../../watch/modelnodes.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'modelnodes.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_ModelNodes[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      23,   12,   11,   11, 0x05,

       0        // eod
};

static const char qt_meta_stringdata_ModelNodes[] = {
    "ModelNodes\0\0node,index\0"
    "nodeAdded(ItemNode*,QModelIndex)\0"
};

void ModelNodes::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        ModelNodes *_t = static_cast<ModelNodes *>(_o);
        switch (_id) {
        case 0: _t->nodeAdded((*reinterpret_cast< ItemNode*(*)>(_a[1])),(*reinterpret_cast< const QModelIndex(*)>(_a[2]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData ModelNodes::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject ModelNodes::staticMetaObject = {
    { &ModelItems::staticMetaObject, qt_meta_stringdata_ModelNodes,
      qt_meta_data_ModelNodes, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ModelNodes::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ModelNodes::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ModelNodes::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ModelNodes))
        return static_cast<void*>(const_cast< ModelNodes*>(this));
    return ModelItems::qt_metacast(_clname);
}

int ModelNodes::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = ModelItems::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    }
    return _id;
}

// SIGNAL 0
void ModelNodes::nodeAdded(ItemNode * _t1, const QModelIndex & _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE
