/****************************************************************************
** Meta object code from reading C++ file 'wndtask.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../../watch/wndtask.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'wndtask.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_ButtonMenu[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      12,   11,   11,   11, 0x0a,
      34,   26,   11,   11, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_ButtonMenu[] = {
    "ButtonMenu\0\0pushed_slot()\0i_index\0"
    "launchCmd(int)\0"
};

void ButtonMenu::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        ButtonMenu *_t = static_cast<ButtonMenu *>(_o);
        switch (_id) {
        case 0: _t->pushed_slot(); break;
        case 1: _t->launchCmd((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData ButtonMenu::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject ButtonMenu::staticMetaObject = {
    { &QPushButton::staticMetaObject, qt_meta_stringdata_ButtonMenu,
      qt_meta_data_ButtonMenu, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ButtonMenu::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ButtonMenu::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ButtonMenu::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ButtonMenu))
        return static_cast<void*>(const_cast< ButtonMenu*>(this));
    return QPushButton::qt_metacast(_clname);
}

int ButtonMenu::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QPushButton::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
