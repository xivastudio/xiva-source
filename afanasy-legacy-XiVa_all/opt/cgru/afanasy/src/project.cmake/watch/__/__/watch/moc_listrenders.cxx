/****************************************************************************
** Meta object code from reading C++ file 'listrenders.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../../watch/listrenders.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'listrenders.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_ListRenders[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      27,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      20,   13,   12,   12, 0x08,
      50,   39,   12,   12, 0x08,
     105,   85,   12,   12, 0x08,
     153,   12,   12,   12, 0x08,
     167,   12,   12,   12, 0x08,
     181,   12,   12,   12, 0x08,
     192,   12,   12,   12, 0x08,
     203,   12,   12,   12, 0x08,
     213,   12,   12,   12, 0x08,
     223,   12,   12,   12, 0x08,
     239,   12,   12,   12, 0x08,
     260,   12,   12,   12, 0x08,
     277,   12,   12,   12, 0x08,
     296,   12,   12,   12, 0x08,
     316,   12,   12,   12, 0x08,
     337,   12,   12,   12, 0x08,
     352,   12,   12,   12, 0x08,
     369,   12,   12,   12, 0x08,
     385,   12,   12,   12, 0x08,
     406,   12,   12,   12, 0x08,
     416,   12,   12,   12, 0x08,
     435,  428,   12,   12, 0x08,
     451,   12,   12,   12, 0x08,
     463,   12,   12,   12, 0x08,
     477,   12,   12,   12, 0x08,
     491,   12,   12,   12, 0x08,
     504,   12,   12,   12, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_ListRenders[] = {
    "ListRenders\0\0i_size\0actChangeSize(int)\0"
    "node,index\0renderAdded(ItemNode*,QModelIndex)\0"
    "selected,deselected\0"
    "selectionChanged(QItemSelection,QItemSelection)\0"
    "actCapacity()\0actMaxTasks()\0actNIMBY()\0"
    "actNimby()\0actFree()\0actUser()\0"
    "actRequestLog()\0actRequestTasksLog()\0"
    "actRequestInfo()\0actEnableService()\0"
    "actDisableService()\0actRestoreDefaults()\0"
    "actSetHidden()\0actUnsetHidden()\0"
    "actEjectTasks()\0actEjectNotMyTasks()\0"
    "actExit()\0actDelete()\0number\0"
    "actCommand(int)\0actReboot()\0actShutdown()\0"
    "actWOLSleep()\0actWOLWake()\0"
    "requestResources()\0"
};

void ListRenders::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        ListRenders *_t = static_cast<ListRenders *>(_o);
        switch (_id) {
        case 0: _t->actChangeSize((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->renderAdded((*reinterpret_cast< ItemNode*(*)>(_a[1])),(*reinterpret_cast< const QModelIndex(*)>(_a[2]))); break;
        case 2: _t->selectionChanged((*reinterpret_cast< const QItemSelection(*)>(_a[1])),(*reinterpret_cast< const QItemSelection(*)>(_a[2]))); break;
        case 3: _t->actCapacity(); break;
        case 4: _t->actMaxTasks(); break;
        case 5: _t->actNIMBY(); break;
        case 6: _t->actNimby(); break;
        case 7: _t->actFree(); break;
        case 8: _t->actUser(); break;
        case 9: _t->actRequestLog(); break;
        case 10: _t->actRequestTasksLog(); break;
        case 11: _t->actRequestInfo(); break;
        case 12: _t->actEnableService(); break;
        case 13: _t->actDisableService(); break;
        case 14: _t->actRestoreDefaults(); break;
        case 15: _t->actSetHidden(); break;
        case 16: _t->actUnsetHidden(); break;
        case 17: _t->actEjectTasks(); break;
        case 18: _t->actEjectNotMyTasks(); break;
        case 19: _t->actExit(); break;
        case 20: _t->actDelete(); break;
        case 21: _t->actCommand((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 22: _t->actReboot(); break;
        case 23: _t->actShutdown(); break;
        case 24: _t->actWOLSleep(); break;
        case 25: _t->actWOLWake(); break;
        case 26: _t->requestResources(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData ListRenders::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject ListRenders::staticMetaObject = {
    { &ListNodes::staticMetaObject, qt_meta_stringdata_ListRenders,
      qt_meta_data_ListRenders, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ListRenders::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ListRenders::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ListRenders::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ListRenders))
        return static_cast<void*>(const_cast< ListRenders*>(this));
    return ListNodes::qt_metacast(_clname);
}

int ListRenders::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = ListNodes::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 27)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 27;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
