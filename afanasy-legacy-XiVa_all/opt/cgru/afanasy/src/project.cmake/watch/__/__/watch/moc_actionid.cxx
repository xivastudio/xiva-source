/****************************************************************************
** Meta object code from reading C++ file 'actionid.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../../watch/actionid.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'actionid.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_ActionId[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      10,    9,    9,    9, 0x05,

 // slots: signature, parameters, type, tag, flags
      27,    9,    9,    9, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_ActionId[] = {
    "ActionId\0\0triggeredId(int)\0"
    "triggeredId_Slot()\0"
};

void ActionId::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        ActionId *_t = static_cast<ActionId *>(_o);
        switch (_id) {
        case 0: _t->triggeredId((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->triggeredId_Slot(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData ActionId::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject ActionId::staticMetaObject = {
    { &QAction::staticMetaObject, qt_meta_stringdata_ActionId,
      qt_meta_data_ActionId, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ActionId::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ActionId::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ActionId::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ActionId))
        return static_cast<void*>(const_cast< ActionId*>(this));
    return QAction::qt_metacast(_clname);
}

int ActionId::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QAction::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    }
    return _id;
}

// SIGNAL 0
void ActionId::triggeredId(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
static const uint qt_meta_data_ActionIdId[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      14,   12,   11,   11, 0x05,

 // slots: signature, parameters, type, tag, flags
      35,   11,   11,   11, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_ActionIdId[] = {
    "ActionIdId\0\0,\0triggeredId(int,int)\0"
    "triggeredId_Slot()\0"
};

void ActionIdId::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        ActionIdId *_t = static_cast<ActionIdId *>(_o);
        switch (_id) {
        case 0: _t->triggeredId((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 1: _t->triggeredId_Slot(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData ActionIdId::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject ActionIdId::staticMetaObject = {
    { &QAction::staticMetaObject, qt_meta_stringdata_ActionIdId,
      qt_meta_data_ActionIdId, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ActionIdId::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ActionIdId::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ActionIdId::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ActionIdId))
        return static_cast<void*>(const_cast< ActionIdId*>(this));
    return QAction::qt_metacast(_clname);
}

int ActionIdId::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QAction::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    }
    return _id;
}

// SIGNAL 0
void ActionIdId::triggeredId(int _t1, int _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
static const uint qt_meta_data_ActionIdIdId[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      17,   14,   13,   13, 0x05,

 // slots: signature, parameters, type, tag, flags
      42,   13,   13,   13, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_ActionIdIdId[] = {
    "ActionIdIdId\0\0,,\0triggeredId(int,int,int)\0"
    "triggeredId_Slot()\0"
};

void ActionIdIdId::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        ActionIdIdId *_t = static_cast<ActionIdIdId *>(_o);
        switch (_id) {
        case 0: _t->triggeredId((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 1: _t->triggeredId_Slot(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData ActionIdIdId::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject ActionIdIdId::staticMetaObject = {
    { &QAction::staticMetaObject, qt_meta_stringdata_ActionIdIdId,
      qt_meta_data_ActionIdIdId, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ActionIdIdId::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ActionIdIdId::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ActionIdIdId::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ActionIdIdId))
        return static_cast<void*>(const_cast< ActionIdIdId*>(this));
    return QAction::qt_metacast(_clname);
}

int ActionIdIdId::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QAction::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    }
    return _id;
}

// SIGNAL 0
void ActionIdIdId::triggeredId(int _t1, int _t2, int _t3)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
static const uint qt_meta_data_ActionString[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      14,   13,   13,   13, 0x05,

 // slots: signature, parameters, type, tag, flags
      39,   13,   13,   13, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_ActionString[] = {
    "ActionString\0\0triggeredString(QString)\0"
    "triggeredString_Slot()\0"
};

void ActionString::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        ActionString *_t = static_cast<ActionString *>(_o);
        switch (_id) {
        case 0: _t->triggeredString((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 1: _t->triggeredString_Slot(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData ActionString::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject ActionString::staticMetaObject = {
    { &QAction::staticMetaObject, qt_meta_stringdata_ActionString,
      qt_meta_data_ActionString, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ActionString::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ActionString::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ActionString::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ActionString))
        return static_cast<void*>(const_cast< ActionString*>(this));
    return QAction::qt_metacast(_clname);
}

int ActionString::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QAction::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    }
    return _id;
}

// SIGNAL 0
void ActionString::triggeredString(QString _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
static const uint qt_meta_data_ActionIdString[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      18,   16,   15,   15, 0x05,

 // slots: signature, parameters, type, tag, flags
      43,   15,   15,   15, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_ActionIdString[] = {
    "ActionIdString\0\0,\0triggeredId(int,QString)\0"
    "triggeredId_Slot()\0"
};

void ActionIdString::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        ActionIdString *_t = static_cast<ActionIdString *>(_o);
        switch (_id) {
        case 0: _t->triggeredId((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 1: _t->triggeredId_Slot(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData ActionIdString::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject ActionIdString::staticMetaObject = {
    { &QAction::staticMetaObject, qt_meta_stringdata_ActionIdString,
      qt_meta_data_ActionIdString, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ActionIdString::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ActionIdString::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ActionIdString::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ActionIdString))
        return static_cast<void*>(const_cast< ActionIdString*>(this));
    return QAction::qt_metacast(_clname);
}

int ActionIdString::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QAction::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    }
    return _id;
}

// SIGNAL 0
void ActionIdString::triggeredId(int _t1, QString _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE
