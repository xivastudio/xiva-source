/****************************************************************************
** Meta object code from reading C++ file 'listusers.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../../watch/listusers.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'listusers.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_ListUsers[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      13,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      22,   11,   10,   10, 0x08,
      55,   10,   10,   10, 0x08,
      70,   10,   10,   10, 0x08,
      92,   10,   10,   10, 0x08,
     113,   10,   10,   10, 0x08,
     134,   10,   10,   10, 0x08,
     152,   10,   10,   10, 0x08,
     172,   10,   10,   10, 0x08,
     195,   10,   10,   10, 0x08,
     213,   10,   10,   10, 0x08,
     229,   10,   10,   10, 0x08,
     251,   10,   10,   10, 0x08,
     274,   10,   10,   10, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_ListUsers[] = {
    "ListUsers\0\0node,index\0"
    "userAdded(ItemNode*,QModelIndex)\0"
    "actHostsMask()\0actHostsMaskExclude()\0"
    "actMaxRunningTasks()\0actErrorsAvoidHost()\0"
    "actErrorRetries()\0actErrorsSameHost()\0"
    "actErrorsForgiveTime()\0actJobsLifeTime()\0"
    "actRequestLog()\0actSolveJobsByOrder()\0"
    "actSolveJobsParallel()\0actDelete()\0"
};

void ListUsers::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        ListUsers *_t = static_cast<ListUsers *>(_o);
        switch (_id) {
        case 0: _t->userAdded((*reinterpret_cast< ItemNode*(*)>(_a[1])),(*reinterpret_cast< const QModelIndex(*)>(_a[2]))); break;
        case 1: _t->actHostsMask(); break;
        case 2: _t->actHostsMaskExclude(); break;
        case 3: _t->actMaxRunningTasks(); break;
        case 4: _t->actErrorsAvoidHost(); break;
        case 5: _t->actErrorRetries(); break;
        case 6: _t->actErrorsSameHost(); break;
        case 7: _t->actErrorsForgiveTime(); break;
        case 8: _t->actJobsLifeTime(); break;
        case 9: _t->actRequestLog(); break;
        case 10: _t->actSolveJobsByOrder(); break;
        case 11: _t->actSolveJobsParallel(); break;
        case 12: _t->actDelete(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData ListUsers::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject ListUsers::staticMetaObject = {
    { &ListNodes::staticMetaObject, qt_meta_stringdata_ListUsers,
      qt_meta_data_ListUsers, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ListUsers::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ListUsers::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ListUsers::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ListUsers))
        return static_cast<void*>(const_cast< ListUsers*>(this));
    return ListNodes::qt_metacast(_clname);
}

int ListUsers::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = ListNodes::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 13)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 13;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
