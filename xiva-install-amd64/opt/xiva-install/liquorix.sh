#!/bin/bash

echo "<?xml version="1.0" encoding="UTF-8" standalone="yes" ?>
<UbuntuAfterInstall>
    <Software>" > xiva-install-liquorix.xml

#liquorix disponiveis p/ instalação
#liqdisp=$(apt search liquorix | grep image | egrep -v "686|modern|64-bit" | cut -d "/" -f 1 | sed '$ d')
#numero do liquorix disponiveis
#liqnum=$(apt search liquorix | grep image | egrep -v "686|modern|64-bit" | grep -c $)
#versão dos liquorix disponiveis
liqver=$(apt search liquorix | grep image | egrep -v "686|modern|64-bit" | cut -d "/" -f 1 | sed '$ d' | cut -d "-" -f 3,4)

#for f in ./*.mp4; do echo "file '$f'" >> mylist.txt; done

for l in $liqver; do echo "		<item>
            <Title>$l</Title>
            <Description>Kernel mono tarefa Liquorix Versão $l</Description>
            <InstallTitle>linux-image-$l-liquorix-amd64 linux-headers-$l-liquorix-amd64</InstallTitle>
            <PPA></PPA>
            <GetAptKey></GetAptKey>
            <AptListEntry></AptListEntry>
            <PreInstall></PreInstall>
            <PostInstall></PostInstall>
            <MinVersion></MinVersion>
        </Item> " >> xiva-install-liquorix.xml; done

echo "    </Software>
</UbuntuAfterInstall>" >> xiva-install-liquorix.xml