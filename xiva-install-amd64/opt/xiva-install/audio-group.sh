#!/bin/bash

exec 2> >(sudo tee -a /var/log/xiva-install-audio-group.log)
exec 1>&2

who=$(who | awk '{ print $1 }' | head -n 1)

#DEBIAN_FRONTEND=noninteractive apt-get -y install qjackctl

echo "@audio   -  rtprio     95" | tee -a /etc/security/limits.conf 
echo "@audio   -  memlock    unlimited" | tee -a /etc/security/limits.conf

usermod -a -G audio $who

zenity --info --title="Ativar JACK" --text="Por Favor reinicie a Sessão para ativar." --width=300 2> /dev/null