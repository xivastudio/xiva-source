#!/bin/sh

if [ "$XDG_CURRENT_DESKTOP" = XFCE ];then
    syncl=$(synclient | grep VertScrollDelt | cut -d "=" -f 2 | sed 's/ /-/')
    synclient VertScrollDelta=$syncl
fi
