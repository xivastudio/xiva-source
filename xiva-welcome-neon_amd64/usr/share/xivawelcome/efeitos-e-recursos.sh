#!/bin/bash

who=$(who | awk '{ print $1 }' | head -n 1)
ans=$(zenity --width=600 --height=250 --title="Recursos e Efeitos XiVa Studio" --list  --text "Quantos recursos e efeitos você gostaria?" --radiolist  --column "Pick" --column "Nº" --column "Tipo" --column Descrição TRUE 1 "Default do XiVa" "Recursos e efeitos padão do XiVa" FALSE 2 "Light" "Muitos recursos e efeitos deligados" FALSE 3 "Ultra-Super-Light   " "Todos os recursos e efeitos desligados" --separator=":")

default () {
    ##configs KWin | espaços de trabalho##
    #comportamente da area de trabalho > contorno de tela
    kwriteconfig5 --file $HOME/.config/kwinrc --group Effect-PresentWindows --key BorderActivateAll "1"
    #ativar transparencia de menus | comportamente da area de trabalho > efeitos da area de trabalho
    kwriteconfig5 --file $HOME/.config/kwinrc --group Effect-kwin4_effect_translucency --key Menus "90"
    #ativar efeitos (plugins)
    kwriteconfig5 --file $HOME/.config/kwinrc --group Plugins --key blurEnabled "true"
    kwriteconfig5 --file $HOME/.config/kwinrc --group Plugins --key contrastEnabled "true"
    kwriteconfig5 --file $HOME/.config/kwinrc --group Plugins --key coverswitchEnabled "true"
    kwriteconfig5 --file $HOME/.config/kwinrc --group Plugins --key cubeEnabled "true"
    kwriteconfig5 --file $HOME/.config/kwinrc --group Plugins --key cubeslideEnabled "true"
    kwriteconfig5 --file $HOME/.config/kwinrc --group Plugins --key desktopchangeosdEnabled "false"
    kwriteconfig5 --file $HOME/.config/kwinrc --group Plugins --key desktopgridEnabled "true"
    kwriteconfig5 --file $HOME/.config/kwinrc --group Plugins --key flipswitchEnabled "true"
    kwriteconfig5 --file $HOME/.config/kwinrc --group Plugins --key highlightwindowEnabled "true"
    kwriteconfig5 --file $HOME/.config/kwinrc --group Plugins --key kwin4_effect_dialogparentEnabled "true"
    kwriteconfig5 --file $HOME/.config/kwinrc --group Plugins --key kwin4_effect_fadeEnabled "true"
    kwriteconfig5 --file $HOME/.config/kwinrc --group Plugins --key kwin4_effect_fadingpopupsEnabled "true"
    kwriteconfig5 --file $HOME/.config/kwinrc --group Plugins --key kwin4_effect_frozenappEnabled "true"
    kwriteconfig5 --file $HOME/.config/kwinrc --group Plugins --key kwin4_effect_loginEnabled "true"
    kwriteconfig5 --file $HOME/.config/kwinrc --group Plugins --key kwin4_effect_logoutEnabled "true"
    kwriteconfig5 --file $HOME/.config/kwinrc --group Plugins --key kwin4_effect_maximizeEnabled "true"
    kwriteconfig5 --file $HOME/.config/kwinrc --group Plugins --key kwin4_effect_morphingpopupsEnabled "true"
    kwriteconfig5 --file $HOME/.config/kwinrc --group Plugins --key kwin4_effect_translucencyEnabled "true"
    kwriteconfig5 --file $HOME/.config/kwinrc --group Plugins --key kwin4_effect_windowapertureEnabled "true"
    kwriteconfig5 --file $HOME/.config/kwinrc --group Plugins --key minimizeanimationEnabled "true"
    kwriteconfig5 --file $HOME/.config/kwinrc --group Plugins --key presentwindowsEnabled "true"
    kwriteconfig5 --file $HOME/.config/kwinrc --group Plugins --key screenedgeEnabled "true"
    kwriteconfig5 --file $HOME/.config/kwinrc --group Plugins --key slideEnabled "false"
    kwriteconfig5 --file $HOME/.config/kwinrc --group Plugins --key slidingpopupsEnabled "true"
    kwriteconfig5 --file $HOME/.config/kwinrc --group Plugins --key zoomEnabled "true"
    #ativar multidesktop | comportamente da area de trabalho > areas de trabalho virtuais 
    #kwriteconfig5 --file $HOME/.config/kwinrc --group Desktops --key Number "4"
    #ativar "lembrar documentos abertos" | comportamente da area de trabalho > atividades > aba privacidade
    kwriteconfig5 --file $HOME/.config/kactivitymanagerdrc --group Plugins --key org.kde.ActivityManager.ResourceScoringEnabled "true"
    kwriteconfig5 --file $HOME/.config/kactivitymanagerd-pluginsrc --group Plugin-org.kde.ActivityManager.Resources.Scoring --key what-to-remember "0"
    #ativar serviços em 2º plano | inicialização e desligamento > serviços em segundo plano
    kwriteconfig5 --file $HOME/.config/kded5rc --group Module-appmenu --key autoload "true"
    kwriteconfig5 --file $HOME/.config/kded5rc --group Module-baloosearchmodule --key autoload "true"
    kwriteconfig5 --file $HOME/.config/kded5rc --group Module-bluedevil --key autoload "true"
    kwriteconfig5 --file $HOME/.config/kded5rc --group Module-browserintegrationreminder --key autoload "true"
    kwriteconfig5 --file $HOME/.config/kded5rc --group Module-colorcorrectlocationupdater --key autoload "true"
    kwriteconfig5 --file $HOME/.config/kded5rc --group Module-device_automounter --key autoload "false"
    kwriteconfig5 --file $HOME/.config/kded5rc --group Module-freespacenotifier --key autoload "true"
    kwriteconfig5 --file $HOME/.config/kded5rc --group Module-kded_printmanager --key autoload "true"
    kwriteconfig5 --file $HOME/.config/kded5rc --group Module-keyboard --key autoload "true"
    kwriteconfig5 --file $HOME/.config/kded5rc --group Module-khotkeys --key autoload "true"
    kwriteconfig5 --file $HOME/.config/kded5rc --group Module-kscreen --key autoload "true"
    kwriteconfig5 --file $HOME/.config/kded5rc --group Module-ksysguard --key autoload "true"
    kwriteconfig5 --file $HOME/.config/kded5rc --group Module-ktimezoned --key autoload "true"
    kwriteconfig5 --file $HOME/.config/kded5rc --group Module-kwrited --key autoload "true"
    kwriteconfig5 --file $HOME/.config/kded5rc --group Module-networkmanagement --key autoload "true"
    kwriteconfig5 --file $HOME/.config/kded5rc --group Module-networkstatus --key autoload "true"
    kwriteconfig5 --file $HOME/.config/kded5rc --group Module-plasmavault --key autoload "true"
    kwriteconfig5 --file $HOME/.config/kded5rc --group Module-proxyscout --key autoload "true"
    kwriteconfig5 --file $HOME/.config/kded5rc --group Module-remotenotifier --key autoload "true"
    kwriteconfig5 --file $HOME/.config/kded5rc --group Module-statusnotifierwatcher --key autoload "true"
    kwriteconfig5 --file $HOME/.config/kded5rc --group Module-touchpad --key autoload "true"
    kwriteconfig5 --file $HOME/.config/kded5rc --group Module-wacomtablet --key autoload "true"
    kwriteconfig5 --file $HOME/.config/kded5rc --group PlasmaBrowserIntegration --key shownCount "1"
    #ativar baloo | pesquisa > pesquisa de arquivos
    balooctl enable
    #ativar | pesquisa > pesquisa plasma
    kwriteconfig5 --file $HOME/.config/krunnerrc --group Plugins --key CharacterRunnerEnabled "true"
    kwriteconfig5 --file $HOME/.config/krunnerrc --group Plugins --key DictionaryEnabled "true"
    kwriteconfig5 --file $HOME/.config/krunnerrc --group Plugins --key Kill RunnerEnabled "true"
    kwriteconfig5 --file $HOME/.config/krunnerrc --group Plugins --key PowerDevilEnabled "true"
    kwriteconfig5 --file $HOME/.config/krunnerrc --group Plugins --key Spell CheckerEnabled "true"
    kwriteconfig5 --file $HOME/.config/krunnerrc --group Plugins --key baloosearchEnabled "true"
    kwriteconfig5 --file $HOME/.config/krunnerrc --group Plugins --key bookmarksEnabled "true"
    kwriteconfig5 --file $HOME/.config/krunnerrc --group Plugins --key browsertabsEnabled "true"
    kwriteconfig5 --file $HOME/.config/krunnerrc --group Plugins --key calculatorEnabled "true"
    kwriteconfig5 --file $HOME/.config/krunnerrc --group Plugins --key desktopsessionsEnabled "true"
    kwriteconfig5 --file $HOME/.config/krunnerrc --group Plugins --key katesessionsEnabled "true"
    kwriteconfig5 --file $HOME/.config/krunnerrc --group Plugins --key konsoleprofilesEnabled "true"
    kwriteconfig5 --file $HOME/.config/krunnerrc --group Plugins --key krunner_appstreamEnabled "true"
    kwriteconfig5 --file $HOME/.config/krunnerrc --group Plugins --key kwinEnabled "true"
    kwriteconfig5 --file $HOME/.config/krunnerrc --group Plugins --key locationsEnabled "true"
    kwriteconfig5 --file $HOME/.config/krunnerrc --group Plugins --key org.kde.activitiesEnabled "true"
    kwriteconfig5 --file $HOME/.config/krunnerrc --group Plugins --key org.kde.datetimeEnabled "true"
    kwriteconfig5 --file $HOME/.config/krunnerrc --group Plugins --key org.kde.windowedwidgetsEnabled "true"
    kwriteconfig5 --file $HOME/.config/krunnerrc --group Plugins --key placesEnabled "true"
    kwriteconfig5 --file $HOME/.config/krunnerrc --group Plugins --key plasma-desktopEnabled "true"
    kwriteconfig5 --file $HOME/.config/krunnerrc --group Plugins --key recentdocumentsEnabled "true"
    kwriteconfig5 --file $HOME/.config/krunnerrc --group Plugins --key servicesEnabled "true"
    kwriteconfig5 --file $HOME/.config/krunnerrc --group Plugins --key shellEnabled "true"
    kwriteconfig5 --file $HOME/.config/krunnerrc --group Plugins --key unitconverterEnabled "true"
    kwriteconfig5 --file $HOME/.config/krunnerrc --group Plugins --key webshortcutsEnabled "true"
    kwriteconfig5 --file $HOME/.config/krunnerrc --group Plugins --key windowsEnabled "true"
    #ativar compositor | tela e monitor > compositor
    kwriteconfig5 --file $HOME/.config/kwinrc --group Compositing --key Enabled "true"
    kwriteconfig5 --file $HOME/.config/kwinrc --group Compositing --key AnimationSpeed "3"
    qdbus org.kde.KWin /Compositor org.kde.kwin.Compositing.resume
    qdbus org.kde.KWin /Compositor org.kde.kwin.Compositing.active
    
    ###aplicar mudanças###
    #KWin
    qdbus org.kde.KWin /KWin reconfigure
    #nohup setsid kwin_x11 --replace &
    kquitapp5 plasmashell && kstart5 plasmashell
    #Aviso p/ Reiniciar Sessão
    zenity --width=400 --info --title="Reiniciar Sessão" --text="Reinicie a Sessão ou Reinicie o Computador para completar as mudanças." 2> /dev/null
}
    
light () {
    ##configs KWin | espaços de trabalho##
    #comportamente da area de trabalho > contorno de tela
    kwriteconfig5 --file $HOME/.config/kwinrc --group Effect-PresentWindows --key BorderActivateAll "9"
    #desativar transparencia de menus | comportamente da area de trabalho > efeitos da area de trabalho
    kwriteconfig5 --file $HOME/.config/kwinrc --group Effect-kwin4_effect_translucency --key Menus ""
    #desativar efeitos (plugins)
    kwriteconfig5 --file $HOME/.config/kwinrc --group Plugins --key blurEnabled "false"
    kwriteconfig5 --file $HOME/.config/kwinrc --group Plugins --key contrastEnabled "false"
    kwriteconfig5 --file $HOME/.config/kwinrc --group Plugins --key coverswitchEnabled "false"
    kwriteconfig5 --file $HOME/.config/kwinrc --group Plugins --key cubeEnabled "false"
    kwriteconfig5 --file $HOME/.config/kwinrc --group Plugins --key cubeslideEnabled "false"
    kwriteconfig5 --file $HOME/.config/kwinrc --group Plugins --key desktopchangeosdEnabled "false"
    kwriteconfig5 --file $HOME/.config/kwinrc --group Plugins --key desktopgridEnabled "false"
    kwriteconfig5 --file $HOME/.config/kwinrc --group Plugins --key flipswitchEnabled "false"
    kwriteconfig5 --file $HOME/.config/kwinrc --group Plugins --key highlightwindowEnabled "true"
    kwriteconfig5 --file $HOME/.config/kwinrc --group Plugins --key kwin4_effect_dialogparentEnabled "false"
    kwriteconfig5 --file $HOME/.config/kwinrc --group Plugins --key kwin4_effect_fadeEnabled "false"
    kwriteconfig5 --file $HOME/.config/kwinrc --group Plugins --key kwin4_effect_fadingpopupsEnabled "false"
    kwriteconfig5 --file $HOME/.config/kwinrc --group Plugins --key kwin4_effect_frozenappEnabled "false"
    kwriteconfig5 --file $HOME/.config/kwinrc --group Plugins --key kwin4_effect_loginEnabled "false"
    kwriteconfig5 --file $HOME/.config/kwinrc --group Plugins --key kwin4_effect_logoutEnabled "false"
    kwriteconfig5 --file $HOME/.config/kwinrc --group Plugins --key kwin4_effect_maximizeEnabled "false"
    kwriteconfig5 --file $HOME/.config/kwinrc --group Plugins --key kwin4_effect_morphingpopupsEnabled "false"
    kwriteconfig5 --file $HOME/.config/kwinrc --group Plugins --key kwin4_effect_translucencyEnabled "false"
    kwriteconfig5 --file $HOME/.config/kwinrc --group Plugins --key kwin4_effect_windowapertureEnabled "false"
    kwriteconfig5 --file $HOME/.config/kwinrc --group Plugins --key minimizeanimationEnabled "false"
    kwriteconfig5 --file $HOME/.config/kwinrc --group Plugins --key presentwindowsEnabled "false"
    kwriteconfig5 --file $HOME/.config/kwinrc --group Plugins --key screenedgeEnabled "false"
    kwriteconfig5 --file $HOME/.config/kwinrc --group Plugins --key slideEnabled "false"
    kwriteconfig5 --file $HOME/.config/kwinrc --group Plugins --key slidingpopupsEnabled "false"
    kwriteconfig5 --file $HOME/.config/kwinrc --group Plugins --key zoomEnabled "false"
    #desativar multidesktop | comportamente da area de trabalho > areas de trabalho virtuais 
    #kwriteconfig5 --file $HOME/.config/kwinrc --group Desktops --key Number "1"
    #desativar "lembrar documentos abertos" | comportamente da area de trabalho > atividades > aba privacidade
    kwriteconfig5 --file $HOME/.config/kactivitymanagerdrc --group Plugins --key org.kde.ActivityManager.ResourceScoringEnabled "false"
    kwriteconfig5 --file $HOME/.config/kactivitymanagerd-pluginsrc --group Plugin-org.kde.ActivityManager.Resources.Scoring --key what-to-remember "2"
    #desativar serviços em 2º plano | inicialização e desligamento > serviços em segundo plano
    kwriteconfig5 --file $HOME/.config/kded5rc --group Module-appmenu --key autoload "true"
    kwriteconfig5 --file $HOME/.config/kded5rc --group Module-baloosearchmodule --key autoload "false"
    kwriteconfig5 --file $HOME/.config/kded5rc --group Module-bluedevil --key autoload "true"
    kwriteconfig5 --file $HOME/.config/kded5rc --group Module-browserintegrationreminder --key autoload "false"
    kwriteconfig5 --file $HOME/.config/kded5rc --group Module-colorcorrectlocationupdater --key autoload "false"
    kwriteconfig5 --file $HOME/.config/kded5rc --group Module-device_automounter --key autoload "false"
    kwriteconfig5 --file $HOME/.config/kded5rc --group Module-freespacenotifier --key autoload "false"
    kwriteconfig5 --file $HOME/.config/kded5rc --group Module-kded_printmanager --key autoload "true"
    kwriteconfig5 --file $HOME/.config/kded5rc --group Module-keyboard --key autoload "true"
    kwriteconfig5 --file $HOME/.config/kded5rc --group Module-khotkeys --key autoload "true"
    kwriteconfig5 --file $HOME/.config/kded5rc --group Module-kscreen --key autoload "true"
    kwriteconfig5 --file $HOME/.config/kded5rc --group Module-ksysguard --key autoload "false"
    kwriteconfig5 --file $HOME/.config/kded5rc --group Module-ktimezoned --key autoload "true"
    kwriteconfig5 --file $HOME/.config/kded5rc --group Module-kwrited --key autoload "false"
    kwriteconfig5 --file $HOME/.config/kded5rc --group Module-networkmanagement --key autoload "true"
    kwriteconfig5 --file $HOME/.config/kded5rc --group Module-networkstatus --key autoload "true"
    kwriteconfig5 --file $HOME/.config/kded5rc --group Module-plasmavault --key autoload "false"
    kwriteconfig5 --file $HOME/.config/kded5rc --group Module-proxyscout --key autoload "true"
    kwriteconfig5 --file $HOME/.config/kded5rc --group Module-remotenotifier --key autoload "false"
    kwriteconfig5 --file $HOME/.config/kded5rc --group Module-statusnotifierwatcher --key autoload "true"
    kwriteconfig5 --file $HOME/.config/kded5rc --group Module-touchpad --key autoload "true"
    kwriteconfig5 --file $HOME/.config/kded5rc --group Module-wacomtablet --key autoload "true"
    kwriteconfig5 --file $HOME/.config/kded5rc --group PlasmaBrowserIntegration --key shownCount "1"
    #destivar baloo | pesquisa > pesquisa de arquivos
    balooctl disable
    #desativar | pesquisa > pesquisa plasma
    kwriteconfig5 --file $HOME/.config/krunnerrc --group Plugins --key CharacterRunnerEnabled "false"
    kwriteconfig5 --file $HOME/.config/krunnerrc --group Plugins --key DictionaryEnabled "false"
    kwriteconfig5 --file $HOME/.config/krunnerrc --group Plugins --key Kill RunnerEnabled "false"
    kwriteconfig5 --file $HOME/.config/krunnerrc --group Plugins --key PowerDevilEnabled "false"
    kwriteconfig5 --file $HOME/.config/krunnerrc --group Plugins --key Spell CheckerEnabled "false"
    kwriteconfig5 --file $HOME/.config/krunnerrc --group Plugins --key baloosearchEnabled "false"
    kwriteconfig5 --file $HOME/.config/krunnerrc --group Plugins --key bookmarksEnabled "false"
    kwriteconfig5 --file $HOME/.config/krunnerrc --group Plugins --key browsertabsEnabled "false"
    kwriteconfig5 --file $HOME/.config/krunnerrc --group Plugins --key calculatorEnabled "false"
    kwriteconfig5 --file $HOME/.config/krunnerrc --group Plugins --key desktopsessionsEnabled "false"
    kwriteconfig5 --file $HOME/.config/krunnerrc --group Plugins --key katesessionsEnabled "false"
    kwriteconfig5 --file $HOME/.config/krunnerrc --group Plugins --key konsoleprofilesEnabled "false"
    kwriteconfig5 --file $HOME/.config/krunnerrc --group Plugins --key krunner_appstreamEnabled "false"
    kwriteconfig5 --file $HOME/.config/krunnerrc --group Plugins --key kwinEnabled "false"
    kwriteconfig5 --file $HOME/.config/krunnerrc --group Plugins --key locationsEnabled "false"
    kwriteconfig5 --file $HOME/.config/krunnerrc --group Plugins --key org.kde.activitiesEnabled "false"
    kwriteconfig5 --file $HOME/.config/krunnerrc --group Plugins --key org.kde.datetimeEnabled "false"
    kwriteconfig5 --file $HOME/.config/krunnerrc --group Plugins --key org.kde.windowedwidgetsEnabled "false"
    kwriteconfig5 --file $HOME/.config/krunnerrc --group Plugins --key placesEnabled "false"
    kwriteconfig5 --file $HOME/.config/krunnerrc --group Plugins --key plasma-desktopEnabled "false"
    kwriteconfig5 --file $HOME/.config/krunnerrc --group Plugins --key recentdocumentsEnabled "false"
    kwriteconfig5 --file $HOME/.config/krunnerrc --group Plugins --key servicesEnabled "false"
    kwriteconfig5 --file $HOME/.config/krunnerrc --group Plugins --key shellEnabled "false"
    kwriteconfig5 --file $HOME/.config/krunnerrc --group Plugins --key unitconverterEnabled "false"
    kwriteconfig5 --file $HOME/.config/krunnerrc --group Plugins --key webshortcutsEnabled "false"
    kwriteconfig5 --file $HOME/.config/krunnerrc --group Plugins --key windowsEnabled "false"
    #ativar compositor | tela e monitor > compositor
    kwriteconfig5 --file $HOME/.config/kwinrc --group Compositing --key Enabled "true"
    kwriteconfig5 --file $HOME/.config/kwinrc --group Compositing --key AnimationSpeed "0"
    qdbus org.kde.KWin /Compositor org.kde.kwin.Compositing.resume
    qdbus org.kde.KWin /Compositor org.kde.kwin.Compositing.active

    ###aplicar mudanças###
    #KWin
    qdbus org.kde.KWin /KWin reconfigure
    #nohup setsid kwin_x11 --replace &
    kquitapp5 plasmashell && kstart5 plasmashell
    zenity --width=400 --info --title="Reiniciar Sessão" --text="Reinicie a Sessão ou Reinicie o Computador para completar as mudanças." 2> /dev/null
}


ultra () {
    ##configs KWin | espaços de trabalho##
    #comportamente da area de trabalho > contorno de tela
    kwriteconfig5 --file $HOME/.config/kwinrc --group Effect-PresentWindows --key BorderActivateAll "9"
    #desativar transparencia de menus | comportamente da area de trabalho > efeitos da area de trabalho
    kwriteconfig5 --file $HOME/.config/kwinrc --group Effect-kwin4_effect_translucency --key Menus ""
    #desativar efeitos (plugins)
    kwriteconfig5 --file $HOME/.config/kwinrc --group Plugins --key blurEnabled "false"
    kwriteconfig5 --file $HOME/.config/kwinrc --group Plugins --key contrastEnabled "false"
    kwriteconfig5 --file $HOME/.config/kwinrc --group Plugins --key coverswitchEnabled "false"
    kwriteconfig5 --file $HOME/.config/kwinrc --group Plugins --key cubeEnabled "false"
    kwriteconfig5 --file $HOME/.config/kwinrc --group Plugins --key cubeslideEnabled "false"
    kwriteconfig5 --file $HOME/.config/kwinrc --group Plugins --key desktopchangeosdEnabled "false"
    kwriteconfig5 --file $HOME/.config/kwinrc --group Plugins --key desktopgridEnabled "false"
    kwriteconfig5 --file $HOME/.config/kwinrc --group Plugins --key flipswitchEnabled "false"
    kwriteconfig5 --file $HOME/.config/kwinrc --group Plugins --key highlightwindowEnabled "true"
    kwriteconfig5 --file $HOME/.config/kwinrc --group Plugins --key kwin4_effect_dialogparentEnabled "false"
    kwriteconfig5 --file $HOME/.config/kwinrc --group Plugins --key kwin4_effect_fadeEnabled "false"
    kwriteconfig5 --file $HOME/.config/kwinrc --group Plugins --key kwin4_effect_fadingpopupsEnabled "false"
    kwriteconfig5 --file $HOME/.config/kwinrc --group Plugins --key kwin4_effect_frozenappEnabled "false"
    kwriteconfig5 --file $HOME/.config/kwinrc --group Plugins --key kwin4_effect_loginEnabled "false"
    kwriteconfig5 --file $HOME/.config/kwinrc --group Plugins --key kwin4_effect_logoutEnabled "false"
    kwriteconfig5 --file $HOME/.config/kwinrc --group Plugins --key kwin4_effect_maximizeEnabled "false"
    kwriteconfig5 --file $HOME/.config/kwinrc --group Plugins --key kwin4_effect_morphingpopupsEnabled "false"
    kwriteconfig5 --file $HOME/.config/kwinrc --group Plugins --key kwin4_effect_translucencyEnabled "false"
    kwriteconfig5 --file $HOME/.config/kwinrc --group Plugins --key kwin4_effect_windowapertureEnabled "false"
    kwriteconfig5 --file $HOME/.config/kwinrc --group Plugins --key minimizeanimationEnabled "false"
    kwriteconfig5 --file $HOME/.config/kwinrc --group Plugins --key presentwindowsEnabled "false"
    kwriteconfig5 --file $HOME/.config/kwinrc --group Plugins --key screenedgeEnabled "false"
    kwriteconfig5 --file $HOME/.config/kwinrc --group Plugins --key slideEnabled "false"
    kwriteconfig5 --file $HOME/.config/kwinrc --group Plugins --key slidingpopupsEnabled "false"
    kwriteconfig5 --file $HOME/.config/kwinrc --group Plugins --key zoomEnabled "false"
    #desativar multidesktop | comportamente da area de trabalho > areas de trabalho virtuais 
    #kwriteconfig5 --file $HOME/.config/kwinrc --group Desktops --key Number "1"
    #desativar "lembrar documentos abertos" | comportamente da area de trabalho > atividades > aba privacidade
    kwriteconfig5 --file $HOME/.config/kactivitymanagerdrc --group Plugins --key org.kde.ActivityManager.ResourceScoringEnabled "false"
    kwriteconfig5 --file $HOME/.config/kactivitymanagerd-pluginsrc --group Plugin-org.kde.ActivityManager.Resources.Scoring --key what-to-remember "2"
    #desativar serviços em 2º plano | inicialização e desligamento > serviços em segundo plano
    kwriteconfig5 --file $HOME/.config/kded5rc --group Module-appmenu --key autoload "true"
    kwriteconfig5 --file $HOME/.config/kded5rc --group Module-baloosearchmodule --key autoload "false"
    kwriteconfig5 --file $HOME/.config/kded5rc --group Module-bluedevil --key autoload "true"
    kwriteconfig5 --file $HOME/.config/kded5rc --group Module-browserintegrationreminder --key autoload "false"
    kwriteconfig5 --file $HOME/.config/kded5rc --group Module-colorcorrectlocationupdater --key autoload "false"
    kwriteconfig5 --file $HOME/.config/kded5rc --group Module-device_automounter --key autoload "false"
    kwriteconfig5 --file $HOME/.config/kded5rc --group Module-freespacenotifier --key autoload "false"
    kwriteconfig5 --file $HOME/.config/kded5rc --group Module-kded_printmanager --key autoload "true"
    kwriteconfig5 --file $HOME/.config/kded5rc --group Module-keyboard --key autoload "true"
    kwriteconfig5 --file $HOME/.config/kded5rc --group Module-khotkeys --key autoload "true"
    kwriteconfig5 --file $HOME/.config/kded5rc --group Module-kscreen --key autoload "true"
    kwriteconfig5 --file $HOME/.config/kded5rc --group Module-ksysguard --key autoload "false"
    kwriteconfig5 --file $HOME/.config/kded5rc --group Module-ktimezoned --key autoload "true"
    kwriteconfig5 --file $HOME/.config/kded5rc --group Module-kwrited --key autoload "false"
    kwriteconfig5 --file $HOME/.config/kded5rc --group Module-networkmanagement --key autoload "true"
    kwriteconfig5 --file $HOME/.config/kded5rc --group Module-networkstatus --key autoload "true"
    kwriteconfig5 --file $HOME/.config/kded5rc --group Module-plasmavault --key autoload "false"
    kwriteconfig5 --file $HOME/.config/kded5rc --group Module-proxyscout --key autoload "true"
    kwriteconfig5 --file $HOME/.config/kded5rc --group Module-remotenotifier --key autoload "false"
    kwriteconfig5 --file $HOME/.config/kded5rc --group Module-statusnotifierwatcher --key autoload "true"
    kwriteconfig5 --file $HOME/.config/kded5rc --group Module-touchpad --key autoload "true"
    kwriteconfig5 --file $HOME/.config/kded5rc --group Module-wacomtablet --key autoload "true"
    kwriteconfig5 --file $HOME/.config/kded5rc --group PlasmaBrowserIntegration --key shownCount "1"
    #destivar baloo | pesquisa > pesquisa de arquivos
    balooctl disable
    #desativar | pesquisa > pesquisa plasma
    kwriteconfig5 --file $HOME/.config/krunnerrc --group Plugins --key CharacterRunnerEnabled "false"
    kwriteconfig5 --file $HOME/.config/krunnerrc --group Plugins --key DictionaryEnabled "false"
    kwriteconfig5 --file $HOME/.config/krunnerrc --group Plugins --key Kill RunnerEnabled "false"
    kwriteconfig5 --file $HOME/.config/krunnerrc --group Plugins --key PowerDevilEnabled "false"
    kwriteconfig5 --file $HOME/.config/krunnerrc --group Plugins --key Spell CheckerEnabled "false"
    kwriteconfig5 --file $HOME/.config/krunnerrc --group Plugins --key baloosearchEnabled "false"
    kwriteconfig5 --file $HOME/.config/krunnerrc --group Plugins --key bookmarksEnabled "false"
    kwriteconfig5 --file $HOME/.config/krunnerrc --group Plugins --key browsertabsEnabled "false"
    kwriteconfig5 --file $HOME/.config/krunnerrc --group Plugins --key calculatorEnabled "false"
    kwriteconfig5 --file $HOME/.config/krunnerrc --group Plugins --key desktopsessionsEnabled "false"
    kwriteconfig5 --file $HOME/.config/krunnerrc --group Plugins --key katesessionsEnabled "false"
    kwriteconfig5 --file $HOME/.config/krunnerrc --group Plugins --key konsoleprofilesEnabled "false"
    kwriteconfig5 --file $HOME/.config/krunnerrc --group Plugins --key krunner_appstreamEnabled "false"
    kwriteconfig5 --file $HOME/.config/krunnerrc --group Plugins --key kwinEnabled "false"
    kwriteconfig5 --file $HOME/.config/krunnerrc --group Plugins --key locationsEnabled "false"
    kwriteconfig5 --file $HOME/.config/krunnerrc --group Plugins --key org.kde.activitiesEnabled "false"
    kwriteconfig5 --file $HOME/.config/krunnerrc --group Plugins --key org.kde.datetimeEnabled "false"
    kwriteconfig5 --file $HOME/.config/krunnerrc --group Plugins --key org.kde.windowedwidgetsEnabled "false"
    kwriteconfig5 --file $HOME/.config/krunnerrc --group Plugins --key placesEnabled "false"
    kwriteconfig5 --file $HOME/.config/krunnerrc --group Plugins --key plasma-desktopEnabled "false"
    kwriteconfig5 --file $HOME/.config/krunnerrc --group Plugins --key recentdocumentsEnabled "false"
    kwriteconfig5 --file $HOME/.config/krunnerrc --group Plugins --key servicesEnabled "false"
    kwriteconfig5 --file $HOME/.config/krunnerrc --group Plugins --key shellEnabled "false"
    kwriteconfig5 --file $HOME/.config/krunnerrc --group Plugins --key unitconverterEnabled "false"
    kwriteconfig5 --file $HOME/.config/krunnerrc --group Plugins --key webshortcutsEnabled "false"
    kwriteconfig5 --file $HOME/.config/krunnerrc --group Plugins --key windowsEnabled "false"
    #desativar compositor
    kwriteconfig5 --file $HOME/.config/kwinrc --group Compositing --key Enabled "false"
    qdbus org.kde.KWin /Compositor org.kde.kwin.Compositing.suspend

    ###aplicar mudanças###
    #KWin
    qdbus org.kde.KWin /KWin reconfigure
    #nohup setsid kwin_x11 --replace &
    kquitapp5 plasmashell && kstart5 plasmashell
    zenity --width=400 --info --title="Reiniciar Sessão" --text="Reinicie a Sessão ou Reinicie o Computador para completar as mudanças." 2> /dev/null
}


case $ans in
    1) default ;;
    2) light ;;
    3) ultra ;;
esac
