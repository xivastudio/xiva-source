#!/bin/bash

lock=$(ps -A | grep apt)

if [ -n "$lock" ]; then
	zenity --width=300 --info --title="Apt Aberto" --text="Algum programa de instalação está aberto no momento, feche-o e tente novamente.\n \n Obrigad@"
	exit 0
fi

rm /etc/apt/sources.list.d/graphics-drivers-ubuntu-ppa-*
rm /etc/apt/sources.list.d/oibaf-ubuntu-graphics-drivers-*

apt update | \
zenity --width=300 --progress \
        --title "Atualização de Fontes" \
        --text="O sistema está atualizando as fontes oficiais.\nAguarde alguns instantes que o instalado de drivers abrira.\nPode demorar alguns minitos dependendo da velocidade da sua conexão com a internet.\n\n Obrigad@\n\n" \
        --pulsate \
        --auto-close \
        --auto-kill

software-properties-gtk --open-tab=4