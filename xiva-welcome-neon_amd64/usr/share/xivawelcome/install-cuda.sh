#!/bin/bash

#criar log em /var/log/xiva-install-cuda.log
exec 2> >(tee -a /var/log/xiva-install-cuda.log)
exec 1>&2

lock=$(ps -A | grep apt)
#verificar conexão com a internet
if [ -n "$lock" ]; then
	zenity --width=300 --info --title="Apt Aberto" --text="Algum programa de instalação está aberto no momento, feche-o e tente novamente.\n \n Obrigad@"
	exit 0
fi

#caso não esteja conectado
net=$(ping -c 1 8.8.8.8 | cut -d " " -f 1 | grep 64)
if [ "$net" != "64" ]; then
	zenity --width=400 --info --title="Conexão com Internet" --text="Não conectado a Internet\nPor favor verifique a sua conexão e tente novamente." 2> /dev/null
	exit 0
fi

zenity --width=400 --question --title="Instalar Aceleração Nvidia CUDA?" --text="Voçê gostaria de ativar os modulos Nvida CUDA?\nIsso vai consumir em media 1GB de Download!" --ok-label="Yes" --cancel-label="No"

if [ $? = 0 ] ; then
	apt update | \
	zenity --width=400 --progress \
        --title "Ativar CUDA" \
 		--text="Atualizando Fontes" \
 		--width="300" \
        --pulsate \
        --auto-close \
        --auto-kill

	DEBIAN_FRONTEND=noninteractive apt install --reinstall nvidia-cuda-toolkit nvidia-modprobe -y | \
	zenity --width=400 --progress \
        --title "Ativar CUDA" \
 		--text="Instalando modulos CUDA" \
 		--width="300" \
        --pulsate \
        --auto-close \
        --auto-kill
	zenity --width=400 --info --title="Instalação Aceleração Nvidia CUDA" --text="Aceleração Nvidia CUDA instalada.\nPor favor reinicie o computador para ter efeito." --width=300 2> /dev/null
fi