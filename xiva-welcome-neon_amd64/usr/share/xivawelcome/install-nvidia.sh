#!/bin/bash

#criar log em /var/log/xiva-install-nvidia.log
exec 2> >(tee -a /var/log/xiva-install-nvidia.log)
exec 1>&2

lock=$(ps -A | grep apt)
if [ -n "$lock" ]; then
	zenity --width=300 --info --title="Apt Aberto" --text="Algum programa de instalação está aberto no momento, feche-o e tente novamente.\n \n Obrigad@"
	exit 0
fi

net=$(ping -c 1 8.8.8.8 | cut -d " " -f 1 | grep 64)

if [ "$net" != "64" ]; then
	zenity --width=300 --info --title="Conexão com Internet" --text="Não conectado a Internet\nPor favor verifique a sua conexão e tente novamente." 2> /dev/null
	exit 0
fi

(
# =================================================================
echo "# Adicionando Fontes" ; sleep 2
rm /etc/apt/sources.list.d/oibaf-ubuntu-graphics-drivers-*
add-apt-repository -y ppa:graphics-drivers/ppa
# Command for first task goes on this line.

# =================================================================
echo "50"
echo "# Atualizando Fontes" ; sleep 2
#apt update
# Command for second task goes on this line.

# =================================================================
echo "99"
echo "# Abrindo gerenciador de Drivers" ; sleep 2
# Command for fifth task goes on this line.

) |
zenity --width=300 --progress \
  --title="Drives Nvidia" \
  --text="First Task." \
  --width="300" \
  --percentage=0 \
  --auto-close \
  --auto-kill

software-properties-gtk --open-tab=4

exit 0
