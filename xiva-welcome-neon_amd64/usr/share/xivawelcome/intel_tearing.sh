#!/bin/sh

#procurar por metodo de aceleração
intel_accel_method_sna=`cat /var/log/Xorg.0.log | grep SNA`

#Configuração perante aceleração encontrada
if [ -n "$intel_accel_method_sna" ];then
	echo "now intel accel mode is : SNA"
	zenity --width=300 --info --title="Aceleração Intel" --text="Aceleração Intel SNA já está instalado.\n"
	intel_accel_method=1
	exit 0
else
	INTEL_XORG_CONF="/etc/X11/xorg.conf.d/20-intel.conf"
	echo 'Section "Device"
	Identifier "Intel Graphics"
	Driver "intel"
	Option "AccelMethod" "sna"
	#Option "PageFlip" "False"
	Option "TearFree" "True"
	EndSection' | sudo tee $INTEL_XORG_CONF
fi

zenity --width=300 --info --title="Aceleração Intel" --text="Reinicie a Sessão para ter efeito.\n"

#sudo systemctl restart lightdm
