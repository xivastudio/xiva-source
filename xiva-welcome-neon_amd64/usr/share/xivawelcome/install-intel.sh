#!/bin/bash

lock=$(ps -A | grep apt)

if [ -n "$lock" ]; then
	zenity --width=300 --info --title="Apt Aberto" --text="Algum programa de instalação está aberto no momento, feche-o e tente novamente.\n \n Obrigad@"
	exit 0
fi

add-apt-repository -y ppa:oibaf/graphics-drivers | \
zenity --width=300 --progress \
        --title "Drives Intel" \
 		--text="Adicionando Fontes" \
 		--width="300" \
        --pulsate \
        --auto-close \
        --auto-kill

#zenity --width=300 --info --title="Intalação Intel Alternativa" --text="O sistema está atualizando as fontes alternetivas.\nAguarde alguns instantes que o instalado de drivers abrira.\nPode demorar alguns minitos dependendo da velocidade da sua conexão com a internet.\n\n Obrigad@"

#apt update
#apt install -y oibaf-graphics-drivers-ppa
#add-apt-repository -y ppa:oibaf/graphics-drivers
#xterm -e "apt update"
#apt upgrade -y
#software-properties-gtk --open-tab=4
plasma-discover --mode update
killall aptd

exit 0