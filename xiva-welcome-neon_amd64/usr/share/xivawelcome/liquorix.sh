#!/bin/bash

#criar log em /var/log/xiva-install-liquorix.log
exec 2> >(tee -a /var/log/xiva-install-liquorix.log)
exec 1>&2

#verificar conexão com a internet
net=$(ping -c 1 8.8.8.8 | cut -d " " -f 1 | grep 64)
#verificar kernel generic instalado
generic=$(ls /boot | grep generic)

#caso não esteja conectado
if [ "$net" != "64" ]; then
	zenity --width=400 --info --title="Conexão com Internet" --text="Não conectado a Internet\nPor favor verifique a sua conexão e tente novamente." 2> /dev/null
	exit 0
fi

#comandos a serem executados
(
# =================================================================
echo "# Atualizando." ; sleep 2
#add ppa do liquorix
add-apt-repository -y ppa:damentz/liquorix
# Command for first task goes on this line.

## =================================================================
#echo "25"
#echo "# Instalando Modo de recuperação.\nEsse operação pode demorar muitos minutos." ; sleep 2
#apt install linux-generic -y
##verificar se generic instalado
#vergen=$(dpkg --list | grep linux-image-generic)
#if [ -z "$vergen" ]; then
#  zenity --width=400 --info --title="ERROR" --text="Algo de errado não deu certo.\nVeja o log em /var/log/xiva-install-liquorix.log\nQualquer duvida entre em contato com o Suporte." 2> /dev/null
#  exit 0
#fi
## Command for second task goes on this line.

# =================================================================
echo "50"
echo "# Instalando Modo Mono-tarefa.\nEsse operação pode demorar muitos minutos." ; sleep 2
apt install -y linux-image-liquorix-amd64 linux-headers-liquorix-amd64
#verificar se liquorix foi instalado
verliq=$(dpkg --list | grep linux-image-liquorix)
if [ -z "$verliq" ]; then
  zenity --width=400 --info --title="ERROR" --text="Algo de errado não deu certo.\nVeja o log em /var/log/xiva-install-liquorix.log\nQualquer duvida entre em contato com o Suporte." 2> /dev/null
  exit 0
fi
# Command for third task goes on this line.

# =================================================================
echo "75"
echo "# Removendo outros Modos." ; sleep 2
#zenity --width=300 --info --title="Instalação Modo do Mono-tarefa" --text="Por favor <b>não feche</b> a Janela de texto.\n\n<b>Se aparecer uma tela de pergunta, responda NÃO</b>.\n\nPressione OK para iniciar a remoção de outros Modos." 2> /dev/null
#if [ -n "$mt" ]; then
#	gnome-terminal -- apt remove --purge *lowlatency* -y
#DEBIAN_FRONTEND=noninteractive apt remove --purge *lowlatency* -y
#else
#	zenity --width=300 --info --title="ERROR" --text="Algo deu errado.\nPor Favor entre em contato com o Suporte." 2> /dev/null
#fi
# Command for fourth task goes on this line.

# =================================================================
echo "99"
echo "# Terminado." ; sleep 2
# Command for fifth task goes on this line.

#executar comandos e abrir caixa de progresso
) |
zenity --width=400 --progress \
  --title="Instalando Modo Mono-tarefa" \
  --text="First Task." \
  --width="400" \
  --percentage=0 \
  --auto-close \
  --auto-kill 

zenity --width=400 --info --title="Instalação Modo Mono-tarefa Real-Time" --text="Modo Mono-tarefa instalado.\nPor favor reinicie o computador para ter efeito." --width=300 2> /dev/null