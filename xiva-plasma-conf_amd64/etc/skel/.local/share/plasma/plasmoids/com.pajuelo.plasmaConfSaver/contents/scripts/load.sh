
configPath=$1
savePath=$2
dataPath=$3
modelData=$4

if [ -d "$savePath/$modelData/icons" ]; then
    mv "$dataPath/icons" "$dataPath/icons.bak"
    cp -r "$savePath/$modelData/icons" "$dataPath"
fi

if [ -d "$savePath/$modelData/color-schemes" ]; then
    mv "$dataPath/color-schemes" "$dataPath/color-schemes.bak"
    cp -r "$savePath/$modelData/color-schemes" "$dataPath"
fi

if [ -d "$savePath/$modelData/plasma" ]; then
    mv "$dataPath/plasma" "$dataPath/plasma.bak"
    cp -r "$savePath/$modelData/plasma" "$dataPath"
fi

if [ -d "$savePath/$modelData/wallpapers" ]; then
    mv "$dataPath/wallpapers" "$dataPath/wallpapers.bak"
    cp -r "$savePath/$modelData/wallpapers" "$dataPath"
fi

if [ -d "$savePath/$modelData/kfontinst" ]; then
    mv "$dataPath/kfontinst" "$dataPath/kfontinst.bak"
    cp -r "$savePath/$modelData/kfontinst" "$dataPath"
fi

#backups
mv "$configPath/plasma-org.kde.plasma.desktop-appletsrc" "$configPath/plasma-org.kde.plasma.desktop-appletsrc.bak"
mv "$configPath/.config/plasmarc" "$configPath/.config/plasmarc.bak"
mv "$configPath/.config/plasmashellrc" "$configPath/.config/plasmashellrc.bak"

# plasma config files
cp "$savePath/$modelData/plasma-org.kde.plasma.desktop-appletsrc" "$configPath/plasma-org.kde.plasma.desktop-appletsrc"
cp "$savePath/$modelData/plasmarc" "$configPath/plasmarc"
cp "$savePath/$modelData/plasmashellrc" "$configPath/plasmashellrc"
cp "$savePath/$modelData/kdeglobals" "$configPath/kdeglobals"
cp "$savePath/$modelData/systemsettingsrc" "$configPath/systemsettingsrc"

#kwin
cp "$savePath/$modelData/kwinrc" "$configPath/kwinrc"
cp "$savePath/$modelData/kwinrulesrc" "$configPath/kwinrulesrc"

#latte-dock config files
mv "$configPath/lattedockrc" "$configPath/lattedockrc.bak"
mv "$configPath/latte" "$configPath/latte.bak"
mv "$configPath/autostart" "$configPath/autostart.bak"
cp "$savePath/$modelData/lattedockrc" "$configPath/lattedockrc"
cp -r "$savePath/$modelData/latte" "$configPath"
cp -r "$savePath/$modelData/autostart" "$configPath"

FILE=$savePath/$modelData/latterun

#dolphin config
cp "$savePath/$modelData/dolphinrc" "$configPath/dolphinrc"
#config session desktop
cp "$savePath/$modelData/ksmserverrc" "$configPath/ksmserverrc"
#config input devices
cp "$savePath/$modelData/kcminputrc" "$configPath/kcminputrc"
#shortcuts
cp "$savePath/$modelData/kglobalshortcutsrc" "$configPath/kglobalshortcutsrc"
cp "$savePath/$modelData/khotkeysrc" "$configPath/khotkeysrc"
#klipper config
cp "$savePath/$modelData/klipperrc" "$configPath/klipperrc"
#konsole config
cp "$savePath/$modelData/konsolerc" "$configPath/konsolerc"
#kscreenlocker config
cp "$savePath/$modelData/kscreenlockerrc" "$configPath/kscreenlockerrc"
#krunner config
cp "$savePath/$modelData/krunnerrc" "$configPath/krunnerrc"

#change the activity ID within the configuration files
kactivities=$(kactivities-cli --current-activity | awk '{print $2}')
find $configPath -maxdepth 2 -type f -exec sed -i "s/activityId=.*/activityId=$kactivities/" {} +

sync
if [ -f "$FILE" ]; then
    killall latte-dock 
    sleep 1 && nohup latte-dock &
else 
    killall latte-dock
    echo Hidden=true | tee -a "$configPath/autostart/org.kde.latte-dock.desktop"
    #permanently disable latte
    find "$configPath/latte/" -type f -iname "*.latte" -exec sed -i 's/isInLatteDock=true//' {} +
    echo Hidden=true >> "$configPath/autostart/org.kde.latte-dock.desktop"
fi

#change generic user name to a real user name within the configuration files
find $configPath -maxdepth 2 -type f -exec sed -i "s/usrconfsaver/$USER/" {} +

qdbus org.kde.KWin /KWin reconfigure 
konsole -e kquitapp5 plasmashell && kstart5 plasmashell --windowclass plasmashell --window Desktop
