#!/bin/bash

#improvement performance
#list (use "whith spaces")
sysctl=(
vm.swappiness=20
"fs.inotify.max_user_watches = 524288"
dev.hpet.max-user-freq=3072
net.ipv4.tcp_syncookies=1
net.ipv4.ip_forward=1
net.ipv4.tcp_dsack=0
net.ipv4.tcp_sack=0
fs.file-max=100000
kernel.sched_migration_cost_ns=5000000
kernel.sched_autogroup_enabled=0
vm.dirty_background_bytes=16777216
vm.dirty_bytes=50331648
kernel.pid_max=4194304
vm.oom_kill_allocating_task=1
)
#if list does not exist in file, type
for i in "${sysctl[@]}"; do if [ -z "$(grep "$i" /etc/sysctl.conf)" ]; then echo "$i" | tee -a "/etc/sysctl.conf";fi ; done

#list (use "whith spaces")
limits=(
"hard stack unlimited"
"nproc unlimited"
"nofile 1048576"
"memlock unlimited"
"as unlimited"
"cpu unlimited"
"fsize unlimited"
"msgqueue unlimited"
"locks unlimited"
"* hard nofile 1048576"
)
#if list does not exist in file, type
for i in "${limits[@]}"; do if [ -z "$(grep "$i" /etc/security/limits.conf)" ]; then echo "$i" | tee -a "/etc/security/limits.conf";fi ; done
